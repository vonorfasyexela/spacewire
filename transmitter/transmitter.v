module transmitter #( 
    
        parameter gInitializeTransmitClockDivideValue = 6'b001001 
    
    ) (

        input transmitClock,
        input clock,
        input receiveClock,
        input reset,

        output spaceWireDataOut,
        output spaceWireStrobeOut,

        input tickIn,
        input [5:0] timeIn,
        input [1:0] controlFlagsIn,
        input transmitDataEnable,
        input [7:0] transmitData,
        input transmitDataControlFlag,

        output transmitReady,

        input enableTransmit,
        input sendNulls,
        input sendFCTs,
        input sendNCharacters,
        input sendTimeCodes,
        input gotFCT,
        input gotNCharacter,
        input [5:0] receiveFIFOCount,
        
        output creditError,

        input [5:0] transmitClockDivide,
        
        output [5:0] creditCountOut,
        output [5:0] outstandingCountOut,
        
        input spaceWireResetOut,

        output transmitEEPAsynchronous,
        output transmitEOPAsynchronous,
        output transmitByteAsynchronous

    );

    // state machine
    localparam [1:0]                                    transmitStateStop = 0,
                                                      transmitStateParity = 1,
                                                     transmitStateControl = 2,
                                                        transmitStateData = 3;

    reg [1:0] transmitState;

    // regs
    reg [5:0] iDivideCount;
    reg iDivideState;
    reg iTransmitParity;
    reg iNullSend;
    reg iTimeCodeSend;
    reg iDataOutRegister;
    reg iStrobeOutRegister;
    reg iSendStart;
    reg iSendDone;
    reg [8:0] iSendData;
    reg [3:0] iSendCount;
    wire transmitDataEnableSynchronized;
    reg iDecrementCredit;
    reg iTransmitFCTStart;
    reg iTransmitFCTDone;
    wire tickInSynchronized;
    reg iTransmitTimeCodeStart;
    reg iTransmitTimeCodeDone;
    reg iTransmitTimeCodeState;
    wire gotNCharacterSynchronized;
    reg [9:0] iGotNCharacterSynchronizedDelay;
    reg [5:0] iOutstandingCount;
    reg [5:0] iReceiveFIFOCountBuffer0;
    reg [5:0] iReceiveFIFOCountBuffer1;
    reg [5:0] iReceiveFIFOCountBuffer;
    reg iTransmitFCTState;
    reg [7:0] iTransmitDataBuffer;
    reg iTransmitDataControlFlagBuffer;
    wire gotFCTSynchronized;
    reg [6:0] iTransmitCreditCount;
    reg iCreditErrorNCharactorOverFlow;
    reg iCreditErrorFCTOverFlow;
    wire iTransmitReady;
    wire iCreditError;
    reg [5:0] iTimeInBuffer = 6'b000000;
    reg iFirstNullSend;
    wire iResetIn;
    reg [5:0] iClockDivideRegister;
    reg iTransmitEEPAsynchronous;
    reg iTransmitEOPAsynchronous;
    reg iTransmitByteAsynchronous;
    reg iCreditOverFlow;

    // assign signals
    assign iResetIn = reset || spaceWireResetOut;
    assign transmitEEPAsynchronous = iTransmitEEPAsynchronous;
    assign transmitEOPAsynchronous = iTransmitEOPAsynchronous;
    assign transmitByteAsynchronous = iTransmitByteAsynchronous;
    assign creditError = iCreditError;
    assign iCreditError = iCreditErrorNCharactorOverFlow ||
                          iCreditErrorFCTOverFlow;
    assign creditCountOut = iTransmitCreditCount[5:0];
    assign outstandingCountOut = iOutstandingCount;
    assign transmitReady = iTransmitReady;
    assign iTransmitReady = ( iSendStart == 1 || 
                              iTransmitFCTStart == 1 ||
                              iTransmitCreditCount == 7'b0000000 ) ? 0 : 1;
    assign spaceWireDataOut = iDataOutRegister;
    assign spaceWireStrobeOut = iStrobeOutRegister;


    // instantiate synchronizers
    synchronize_one_pulse _TRANSMIT_DATA_ENABLE_PULSE (

        .clock( transmitClock ),
        // ???
        .asynchronousClock( clock ),
        .reset( iResetIn ),
        .asynchronousIn( transmitDataEnable ),

        .synchronizedOut( transmitDataEnableSynchronized )

    );

    synchronize_one_pulse _TICK_IN_PULSE (

        .clock( transmitClock ),
        // ???
        .asynchronousClock( clock ),
        .reset( iResetIn ),
        .asynchronousIn( tickIn ),

        .synchronizedOut( tickInSynchronized )

    );

    synchronize_one_pulse _GOT_FCT_PULSE (

        .clock( transmitClock ),
        .asynchronousClock( receiveClock ),
        .reset( iResetIn ),
        .asynchronousIn( gotFCT ),

        .synchronizedOut( gotFCTSynchronized )

    );

    synchronize_one_pulse _GOT_NCHARACTER_PULSE (

        .clock( transmitClock ),
        .asynchronousClock( receiveClock ),
        .reset( iResetIn ),
        .asynchronousIn( gotNCharacter ),

        .synchronizedOut( gotNCharacterSynchronized )

    );

    // Useful tasks
    task parity;
        begin
            if ( iDataOutRegister == iTransmitParity ) begin
                iDataOutRegister = iTransmitParity;
                iStrobeOutRegister = ~iStrobeOutRegister;
            end
            else begin
                iDataOutRegister = iTransmitParity;
                iStrobeOutRegister = iStrobeOutRegister;
            end        
        end
    endtask

    task parity_xor;
        begin
            if ( iDataOutRegister == ( iTransmitParity ^ 1'b1 ) ) begin
                iDataOutRegister   = iTransmitParity ^ 1'b1;
                iStrobeOutRegister = ~iStrobeOutRegister;
            end
            else begin
                iDataOutRegister   = iTransmitParity ^ 1'b1;
                iStrobeOutRegister = iStrobeOutRegister;
            end
            
        end
    endtask

    

    // Statistical information, Transmit One Shot Pulse(EOP,EEP,1Byte)
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            // reset
            iTransmitEEPAsynchronous  = 0;
            iTransmitEOPAsynchronous  = 0;
            iTransmitByteAsynchronous = 0;
        end
        else begin
            if ( transmitDataEnable == 1 ) begin
                if ( transmitDataControlFlag == 1 ) begin
                    if ( transmitData[0] == 0 ) begin
                        // EOP transmit
                        iTransmitEOPAsynchronous = 1;
                    end
                    else begin
                        // EEP transmit
                        iTransmitEEPAsynchronous = 1;
                    end
                end
                else if ( transmitDataControlFlag == 0 ) begin
                    // 1 byte transmit
                    iTransmitByteAsynchronous = 1;
                end
            end
            else begin
                iTransmitEEPAsynchronous  = 0;
                iTransmitEOPAsynchronous  = 0;
                iTransmitByteAsynchronous = 0;     
            end
        end
    end

    // ECSS-E-ST-50-12C 8.4.2 Transmitter
    // When the TICK_IN signal is asserted the transmitter sends out a Time-Code
    // as soon as the transmitter has finished sending the current character or 
    // control code. The value of the Time-Code is the value of the TIME_IN and 
    // CONTROL-FLAGS_IN signals at the point in time when TICK_IN is asserted.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            // reset
            iTransmitTimeCodeState = 0;
            iTransmitTimeCodeStart = 0;
            iTimeInBuffer          = 0;
        end    
        else begin
            if ( sendTimeCodes == 1 ) begin
                if ( iTransmitTimeCodeState == 0 ) begin
                    if ( tickInSynchronized  == 1 ) begin
                        iTransmitTimeCodeStart = 1;
                        iTransmitTimeCodeState = 1;
                        iTimeInBuffer          = timeIn;
                    end
                end
                else begin
                    if ( iTransmitTimeCodeDone == 1 ) begin
                        iTransmitTimeCodeStart = 0;
                        iTransmitTimeCodeState = 0;
                    end
                end
            end
        end
    end
    
    // ECSS-E-ST-50-12C 8.3 Flow control (normative)
    // Receives an FCT its transmitter increments the credit count by eight.
    // Whenever the transmitter sends an N-Char it decrements the credit count 
    // by one.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iTransmitCreditCount = 0;
        end
        else begin
            if ( gotFCTSynchronized == 1 ) begin
                if ( iDecrementCredit == 1 ) begin
                    iTransmitCreditCount = iTransmitCreditCount + 7;
                end
                else begin
                   iTransmitCreditCount = iTransmitCreditCount + 8; 
                end
            end
            else if ( iDecrementCredit == 1 ) begin
                if ( iTransmitCreditCount != 0 ) begin
                    iTransmitCreditCount = iTransmitCreditCount - 1;
                end
            end
        end
    end

    // ECSS-E-ST-50-12C 8.5.3.8 CreditError
    // If an FCT is received when the credit count is at or close to its maximum
    // value (i.e. within eight of the maximum value), the credit count is
    // not incremented and a credit error occurs.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iCreditOverFlow = 0;
        end
        else begin
            if ( iTransmitCreditCount > 7'b0111000 ) begin
                iCreditOverFlow = 1;
            end
        end
    end

    // ECSS-E-ST-50-12C 8.5.3.8 CreditError
    // Synchronized Reset the CreditErrorFCTOverFlow.
    always @( posedge transmitClock ) begin
        if ( iResetIn == 1 ) begin
            iCreditErrorFCTOverFlow = 0;
        end
        else begin
            iCreditErrorFCTOverFlow = iCreditOverFlow & ~iCreditErrorFCTOverFlow;
        end
    end

    // Receive Wait time for subtraction OutstandingCount
    // after adding receiveFIFOCount.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iGotNCharacterSynchronizedDelay = 0;
        end 
        else begin
            iGotNCharacterSynchronizedDelay[0]   = gotNCharacterSynchronized;
            iGotNCharacterSynchronizedDelay[9:1] = iGotNCharacterSynchronizedDelay[8:0];
        end
    end

    // Synchronized input signal to transmitClock. 
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iReceiveFIFOCountBuffer0 = 0;
            iReceiveFIFOCountBuffer1 = 0;
            iReceiveFIFOCountBuffer  = 0;
        end
        else begin
            iReceiveFIFOCountBuffer0 = receiveFIFOCount;
            iReceiveFIFOCountBuffer1 = iReceiveFIFOCountBuffer0;

            if ( iReceiveFIFOCountBuffer1 == iReceiveFIFOCountBuffer0 ) begin
                iReceiveFIFOCountBuffer = iReceiveFIFOCountBuffer1;
            end
        end
    end

    // ECSS-E-ST-50-12C 8.3 Flow control (normative)
    // Each time a link interface receives an FCT its transmitter
    // increments the credit count by eight.
    // Whenever the transmitter sends an N-Char it decrements the credit
    // count by one.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iOutstandingCount              = 0;
            iTransmitFCTState              = 0;
            iTransmitFCTStart              = 0;
            iCreditErrorNCharactorOverFlow = 0;
        end
        else begin
            if ( iTransmitFCTState == 0 ) begin
                if ( ( iOutstandingCount + iReceiveFIFOCountBuffer <= 6'b110000 ) && 
                     ( sendFCTs == 1 ) ) begin
                    iTransmitFCTStart = 1;
                    iTransmitFCTState = 1;
                end

                if ( iGotNCharacterSynchronizedDelay[9] == 1 ) begin
                    iOutstandingCount = iOutstandingCount - 1;
                end
            end
            else begin
                if ( iTransmitFCTDone == 1 ) begin
                    if ( iGotNCharacterSynchronizedDelay[9] == 1 ) begin
                        iOutstandingCount = iOutstandingCount + 7;
                    end
                    else begin
                        iOutstandingCount = iOutstandingCount + 8;
                    end
                    iTransmitFCTStart = 0;
                    iTransmitFCTState = 0;
                end
                else begin
                    if ( iGotNCharacterSynchronizedDelay[9] == 1 ) begin
                        iOutstandingCount = iOutstandingCount - 1;
                    end
                end
            end

            // ECSS-E-ST-50-12C 8.5.3.8 CreditError
            // Credit error occurs if data is received when the
            // host system is not expecting any more data.                   
            if ( iGotNCharacterSynchronizedDelay[9] == 1 &&
                iOutstandingCount == 0 ) begin
                
                iCreditErrorNCharactorOverFlow = 1;        
            end
            else begin
                iCreditErrorNCharactorOverFlow = 0;
            end
        end
    end

    // Instract to start Transmit and load data to buffer after read the data from 
    // TransmitFIFO.
    always @( posedge transmitClock or posedge iResetIn ) begin
        if ( iResetIn ) begin
            iSendStart                     = 0;
            iTransmitDataBuffer            = 0;
            iTransmitDataControlFlagBuffer = 0;
        end
        else begin
            if ( transmitDataEnableSynchronized == 1 ) begin
                iTransmitDataBuffer            = transmitData;
                iTransmitDataControlFlagBuffer = transmitDataControlFlag;
                iSendStart                     = 1;
            end
            else if ( iSendDone == 1 ) begin
                iSendStart = 0;
            end
        end
    end

    // ECSS-E-ST-50-12C 6.6.5 Initial operating data signalling rate
    // After a reset the SpaceWire link transmitter shall initially commence 
    // operating at a data signalling rate of (10±1) Mb/s.
    always @( posedge transmitClock or posedge reset ) begin
        if ( reset ) begin
            iClockDivideRegister = gInitializeTransmitClockDivideValue;
        end
        else begin
            iClockDivideRegister = sendNCharacters ? 
                                   transmitClockDivide : 
                                   gInitializeTransmitClockDivideValue;
        end
    end    

    // ECSS-E-ST-50-12C 8.4.3 Transmit clock
    // Dividing counter to determine the Transmit signalling rate.
    always @( posedge transmitClock or posedge reset ) begin
        if ( reset ) begin
            iDivideCount = 0;
            iDivideState = 0;
        end
        else begin
            if ( iDivideCount >= iClockDivideRegister ) begin
                iDivideCount = 0;
                iDivideState = 1;
            end
            else begin
                iDivideCount = iDivideCount + 1;
                iDivideState = 0;
            end
        end
    end

    always @( posedge transmitClock or posedge reset ) begin
        if ( reset ) begin
            iTransmitParity = 0;
            transmitState = transmitStateStop;
            iDataOutRegister = 0;
            iStrobeOutRegister = 0;
            iNullSend = 0;
            iTimeCodeSend = 0;
            iSendDone = 0;
            iTransmitFCTDone = 0;
            iTransmitTimeCodeDone = 0;
            iDecrementCredit = 0;
            iFirstNullSend = 0;
            iSendCount = 0;
            iSendData = 0;
        end
        else begin
            if ( iDivideState == 1 ) begin
                // statmach

                case ( transmitState )

                    transmitStateStop: begin
                        if ( enableTransmit == 1 && sendNulls == 1 ) begin
                            transmitState = transmitStateParity;
                        end
                        else begin
                            iTransmitParity    = 0;
                            iDataOutRegister   = 0;
                            iStrobeOutRegister = 0;
                            iNullSend          = 0;
                            iFirstNullSend     = 0;
                        end
                    end

                    // odd parity generate
                    transmitStateParity: begin
                        if ( enableTransmit == 1 ) begin
                            
                            if ( iNullSend == 1 ) begin
                                // send pending FCT of NULL(ESC+FCT)
                                iSendData = { 6'b000000, 3'b001 };
                                iSendCount = 4'h2;
                                parity;
                                iNullSend = 0;
                            end

                            else if ( iTimeCodeSend == 1 ) begin
                                // send pending TIME of TCODE(ESC+TIME)
                                iSendData = { controlFlagsIn, iTimeInBuffer, 1'b0 };    
                                iSendCount = 4'h8;
                                parity_xor;
                                iTimeCodeSend         = 0;
                                iTransmitTimeCodeDone = 1;
                            end

                            else if ( iTransmitTimeCodeStart == 1 ) begin
                                // send ESC of TCODE
                                iSendData = { 6'b000000, 3'b111 };
                                iSendCount = 4'h2;
                                parity;
                                iTimeCodeSend = 1;
                            end

                            else if ( sendFCTs == 1 &&
                                      iTransmitFCTStart == 1 && 
                                      iFirstNullSend == 1 ) begin
                                
                                // send FCT
                                iSendData = { 6'b000000, 3'b001 };
                                iSendCount = 4'h2;
                                parity;
                                iTransmitFCTDone = 1;
                            end

                            else if ( sendNCharacters == 1 && iSendStart == 1 ) begin
                                iDecrementCredit = 1;
                                if ( iTransmitDataControlFlagBuffer == 1 && 
                                     iTransmitDataBuffer[0] == 0 ) begin
                                    
                                    // send EOP
                                    iSendData  = { 1'b0, 5'b00000, 3'b101 };
                                    iSendCount = 4'h2;
                                    parity;
                                end
                                else if ( iTransmitDataControlFlagBuffer == 1 &&
                                          iTransmitDataBuffer[0] == 1 ) begin
                                    // send EEP
                                    iSendData = { 1'b0, 5'b00000, 3'b011 };
                                    iSendCount = 4'h2;
                                    parity;              
                                end
                                else begin
                                    // send 8-bit data
                                    iSendData = { iTransmitDataBuffer, 1'b0 };
                                    iSendCount = 4'h8;
                                    parity_xor;
                                end
                                iSendDone = 1;
                            end

                            else if ( sendNulls == 1 ) begin
                                // send ESC of NULL
                                iSendData      = { 6'b000000, 3'b111 };
                                iSendCount     = 4'h2;
                                parity;
                                iNullSend      = 1;
                                iFirstNullSend = 1;
                            end
                            transmitState = transmitStateControl;

                        end
                        else begin
                            iDataOutRegister = 0;
                            transmitState    = transmitStateStop;
                        end
                    end

                    // Transmit Data Control Flag
                    // Data Character = "0" Control Caracter = "1".
                    transmitStateControl: begin
                        if ( enableTransmit == 1 ) begin
                            iSendDone = 0;
                            iDecrementCredit = 0;
                            iTransmitFCTDone = 0;
                            iTransmitTimeCodeDone = 0;
                            iSendCount = iSendCount - 1;
                            if ( iDataOutRegister == iSendData[0] ) begin
                                iDataOutRegister = iSendData[0];
                                iStrobeOutRegister = ~iStrobeOutRegister;
                            end
                            else begin
                                iDataOutRegister = iSendData[0];
                                iStrobeOutRegister = iStrobeOutRegister;
                            end
                            iSendData = { 1'b0, iSendData[8:1] };
                            iTransmitParity = 0;
                            transmitState = transmitStateData;
                        end
                        else begin
                            iDataOutRegister = 0;
                            transmitState = transmitStateStop;
                        end
                    end

                    // Transmit Data Character or Control Character
                    transmitStateData: begin
                        if ( enableTransmit == 1 ) begin
                            if ( iDataOutRegister == iSendData[0] ) begin
                                iDataOutRegister = iSendData[0];
                                iStrobeOutRegister = ~iStrobeOutRegister;
                            end
                            else begin
                                iDataOutRegister = iSendData[0];
                                iStrobeOutRegister = iStrobeOutRegister;
                            end
                            iTransmitParity = iTransmitParity ^ iSendData[0];
                            iSendData = { 1'b0, iSendData[8:1] };
                            if ( iSendCount == 4'b0000 ) begin
                                transmitState = transmitStateParity;
                            end
                            else begin
                                iSendCount = iSendCount - 1;
                            end
                        end
                        else begin
                            iDataOutRegister = 0;
                            transmitState = transmitStateStop;
                        end
                    end
                endcase
            end
            else begin
                iTransmitFCTDone = 0;
                iSendDone        = 0;
                iDecrementCredit = 0;
            end
        end
    end
endmodule