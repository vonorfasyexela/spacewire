module receiver_synchronize #( parameter gDisconnectCountValue = 141 ) (

        input spaceWireStrobeIn,
        input spaceWireDataIn,
        
        output [8:0] receiveDataOut,
        output receiveDataValidOut,
        output [7:0] receiveTimeCodeOut,
        output receiveTimeCodeValidOut,
        output receiveNCharacterOut,
        output receiveFCTOut,
        output receiveNullOut,
        output receiveEEPOut,
        output receiveEOPOut,
        output receiveOffOut,
        output receiverErrorOut,
        output parityErrorOut,
        output escapeErrorOut,
        output disconnectErrorOut,
        
        input spaceWireReset,

        output receiveFIFOWriteEnable,

        input enableReceive,
        input receiveClock

    );

    localparam [2:0]                                        spaceWireIdel = 0,
                                                             spaceWireOff = 1,
                                                           spaceWireEven0 = 2,
                                                           spaceWireEven1 = 3,
                                                        spaceWireWaitEven = 4, 
                                                            spaceWireOdd0 = 5,
                                                            spaceWireOdd1 = 6,
                                                         spaceWireWaitOdd = 7;

    reg [2:0] spaceWireState;

    reg [7:0] iDataRegister;
    reg iParity;
    reg iESCFlag;
    reg [1:0] iSpaceWireSynchronize;
    reg [3:0] iBitCount;
    reg [7:0] iLinkTimeOutCounter;
    reg iDisconnectErrorOut;
    reg iParityErrorOut;
    reg iEscapeErrorOut;
    reg iCommandFlag;
    reg iDataFlag;

    reg iReceiverEOPOut;
    reg iReceiverEEPOut;
    reg iReceiverDataValidOut;

    reg [8:0] iReceiveDataOut = 0;
    reg [7:0] iReceiveTimeCodeOut;
    reg iReceiveTimeCodeValidOut;
    wire iReceiveNCharacterOut;
    reg iReceiveFCTOut;
    reg iReceiveNullOut;
    wire iReceiveOffOut;
    wire iReceiverErrorOut;
    reg iReceiveFIFOWriteEnable;

    // assign signals
    assign receiveDataOut = iReceiveDataOut;
    assign receiveTimeCodeOut = iReceiveTimeCodeOut;
    assign receiveTimeCodeValidOut = iReceiveTimeCodeValidOut;
    assign receiveNCharacterOut = iReceiveNCharacterOut;
    assign receiveFCTOut = iReceiveFCTOut;
    assign receiveNullOut = iReceiveNullOut;
    assign receiveOffOut = iReceiveOffOut;
    assign receiverErrorOut = iReceiverErrorOut;
    assign receiveFIFOWriteEnable = iReceiveFIFOWriteEnable;

    assign iReceiveOffOut = spaceWireState == spaceWireOff ? 1 : 0;
    assign iReceiverErrorOut = ( iDisconnectErrorOut == 1 || 
                                 iParityErrorOut == 1 || 
                                 iEscapeErrorOut == 1 ) ? 1 : 0;
    assign iReceiveNCharacterOut = ( iReceiverEOPOut == 1 || 
                                     iReceiverEEPOut == 1 || 
                                     iReceiverDataValidOut == 1 ) ? 1 : 0;
    assign receiveDataValidOut   = iReceiverDataValidOut;
    assign receiveEOPOut         = iReceiverEOPOut;
    assign receiveEEPOut         = iReceiverEEPOut;
    assign parityErrorOut        = iParityErrorOut;
    assign escapeErrorOut        = iEscapeErrorOut;
    assign disconnectErrorOut    = iDisconnectErrorOut;


    // 
    // ECSS-E-ST-50-12C 8.4.4 Receiver
    // 

    // synchronize DS signal to the receiveClock
    always @( posedge receiveClock ) begin
        iSpaceWireSynchronize = { spaceWireStrobeIn, spaceWireDataIn };
    end    

    // Detect a change of the DS signal
    always @( posedge receiveClock or 
              posedge spaceWireReset or 
              posedge iDisconnectErrorOut ) begin
        // if ( spaceWireReset == 1 || iDisconnectErrorOut == 1 ) begin
        //     spaceWireState = spaceWireIdel;
        // end
        if ( spaceWireReset == 1 ) begin
            spaceWireState = spaceWireIdel;
        end     
        else if ( iDisconnectErrorOut == 1 ) begin
            spaceWireState = spaceWireIdel;
        end
        else begin 
            if ( enableReceive == 1 ) begin
                if ( spaceWireState == spaceWireIdel ) begin
                    if ( iSpaceWireSynchronize == 2'b00 ) begin
                        spaceWireState = spaceWireOff;
                    end
                end
                else if ( spaceWireState == spaceWireOff ) begin
                    if ( iSpaceWireSynchronize == 2'b10 ) begin
                        spaceWireState = spaceWireOdd0;
                    end
                end
                else if ( spaceWireState == spaceWireEven1 ||
                          spaceWireState == spaceWireEven0 ||
                          spaceWireState == spaceWireWaitOdd ) begin
                
                    if ( iSpaceWireSynchronize == 2'b10 ) begin
                        spaceWireState = spaceWireOdd0;
                    end
                    else if ( iSpaceWireSynchronize == 2'b01 ) begin
                        spaceWireState = spaceWireOdd1;
                    end
                    else begin
                        spaceWireState = spaceWireWaitOdd;
                    end
                end
                else if ( spaceWireState == spaceWireOdd1 || 
                          spaceWireState == spaceWireOdd0 || 
                          spaceWireState == spaceWireWaitEven) begin
                
                    if ( iSpaceWireSynchronize == 2'b00 ) begin
                        spaceWireState = spaceWireEven0;
                    end
                    else if ( iSpaceWireSynchronize == 2'b11 ) begin
                        spaceWireState = spaceWireEven1;
                    end
                    else begin
                        spaceWireState = spaceWireWaitEven;
                    end
                end
                else begin
                    spaceWireState = spaceWireIdel;
                end 
            end 
        end
    end

    always @( posedge receiveClock ) begin
        
        // Take the data into the shift register on the State 
        // transition of spaceWireState
        if ( enableReceive == 1 ) begin
            if ( spaceWireState == spaceWireOff ) begin
                iDataRegister = 0;
            end
            else if ( spaceWireState == spaceWireOdd1 ||
                      spaceWireState == spaceWireEven1 ) begin
                
                iDataRegister = { 1'b1, iDataRegister[7:1] };
            end
            else if ( spaceWireState == spaceWireOdd0 ||
                      spaceWireState == spaceWireEven0 ) begin

                iDataRegister = { 1'b0, iDataRegister[7:1] };
            end
        end
        else begin
            iDataRegister = 0;
        end

        // ECSS-E-ST-50-12C 7.4 Parity for error detection
        // Odd Parity
        if ( enableReceive == 1 && 
             iEscapeErrorOut == 0 && 
             iDisconnectErrorOut == 0 ) begin
                    
            if ( spaceWireState == spaceWireOff ) begin
                iParity = 0;
            end
            else if ( iBitCount == 0 && 
                      spaceWireState == 
                      spaceWireEven1 ) begin
                if ( iParity == 1 ) begin
                    iParityErrorOut = 1;
                    iParity         = 0;
                end
            end
            else if ( iBitCount == 0 && 
                      spaceWireState == 
                      spaceWireEven0 ) begin
                if ( iParity == 0 ) begin
                    iParityErrorOut = 1;
                end
                else begin
                    iParity = 0;
                end
            end
            else if ( spaceWireState == spaceWireOdd1 || 
                      spaceWireState == spaceWireEven1 ) begin
            
                iParity = ~iParity;              
            end

        end
        else begin
            iParityErrorOut = 0;
        end

        // ECSS-E-ST-50-12C 8.5.3.7.2 Disconnect error.
        // Disconnect error is an error condition asserted
        // when the length of time since the last transition on
        // the D or S lines was longer than 850 ns nominal.
        if ( enableReceive == 1 &&
             iEscapeErrorOut == 0 &&
             iParityErrorOut == 0 ) begin
           
            if ( spaceWireState == spaceWireWaitOdd || 
                 spaceWireState == spaceWireWaitEven ) begin
            
                if ( iLinkTimeOutCounter < gDisconnectCountValue ) begin
                    iLinkTimeOutCounter = iLinkTimeOutCounter + 1;
                end         
                else begin
                    iDisconnectErrorOut = 1;
                end
            end
            else if ( spaceWireState == spaceWireIdel ) begin
                iLinkTimeOutCounter = 8'h00;
            end
            else if ( spaceWireState == spaceWireOdd1 || 
                      spaceWireState == spaceWireEven1 || 
                      spaceWireState == spaceWireOdd0 || 
                      spaceWireState == spaceWireEven0 ) begin
                iLinkTimeOutCounter = 8'h00;              
            end
        end
        else begin
            iDisconnectErrorOut = 0;
            iLinkTimeOutCounter = 8'h00;
        end

        // ECSS-E-ST-50-12C 4.4 Character level
        // ECSS-E-ST-50-12C 7.2 Data characters
        // Discriminate the data character or the  the control 
        // character by the Data 
        // Control Flag.
        if ( enableReceive == 1 ) begin
            if ( spaceWireState == spaceWireIdel ) begin
                iCommandFlag = 0; iDataFlag = 0;
            end
            else if ( iBitCount == 0 && 
                      spaceWireState == spaceWireEven0 ) begin
                iCommandFlag = 0; iDataFlag = 1;
            end
            else if ( iBitCount == 0 && 
                      spaceWireState == spaceWireEven1 ) begin
                iCommandFlag = 1; iDataFlag = 0;
            end
        end
        else begin
            iCommandFlag = 0; iDataFlag = 0;
        end

        // Increment bit of character corresponding by state transition of 
        // spaceWireState.
        if ( enableReceive == 1 && 
             iEscapeErrorOut == 0 && 
             iDisconnectErrorOut == 0 ) begin
        
            if ( spaceWireState == spaceWireEven1 || 
                 spaceWireState == spaceWireOff ) begin
            
                iBitCount = 4'h0;
            end
            else if ( spaceWireState == spaceWireEven1 ||
                      spaceWireState == spaceWireEven0 ) begin
            
                if ( iBitCount == 1 && iCommandFlag == 1 ) begin
                    iBitCount = 4'h0;
                end              
                else if ( iBitCount == 4 && iCommandFlag == 0 ) begin
                    iBitCount = 4'h0;
                end
                else begin
                    iBitCount = iBitCount + 1;
                end
            end
        end
        else begin
            iBitCount = 4'h0;
        end

        // ECSS-E-ST-50-12C 7.3 Control characters and control codes.
        // Discriminate  Data character, Control code and Time corde, and 
        // write to receive buffer
        if ( enableReceive == 1 ) begin
            if ( iBitCount == 0 && 
                 ( spaceWireState == spaceWireOdd0 || 
                        spaceWireState == spaceWireOdd1 ) ) begin
               
                  if ( iDataFlag == 1 ) begin
                       if ( iESCFlag == 1 ) begin
                           // time code receive
                           iReceiveTimeCodeOut = iDataRegister;
                       end
                       else begin
                           // data receive
                           iReceiveDataOut = { 1'b0, iDataRegister };
                           iReceiveFIFOWriteEnable = 1;
                       end
                   end      

               else if ( iCommandFlag == 1 ) begin
                   if ( iDataRegister[7:6] == 2'b10 ) begin
                       // EOP
                       iReceiveDataOut = { 1'b1, 8'b00000000 };
                   end
                   else if ( iDataRegister[7:6] == 2'b01 ) begin
                       // EEP
                       iReceiveDataOut = { 1'b1, 8'b00000001 };
                   end
               end

               if ( 
                   ( iESCFlag != 1 ) &&
                   ( iDataRegister[7:6] == 2'b10 || 
                     iDataRegister[7:6] == 2'b01 )
                ) begin
                    // EOP EEP Receive
                    iReceiveFIFOWriteEnable = 1;
                end
            end
            else begin
                iReceiveFIFOWriteEnable = 0;
            end
        end

        // ECSS-E-ST-50-12C 7.3 Control characters and control codes.
        // ECSS-E-ST-50-12C 8.5.3.7.4 Escape error.
        // Receive DataCharacter, ControlCode and TimeCode.
        if ( enableReceive == 1 && 
             iDisconnectErrorOut == 0 &&
            iParityErrorOut == 0 ) begin
            
            if ( iBitCount == 0 &&
                 ( spaceWireState == spaceWireOdd0 || 
                   spaceWireState == spaceWireOdd1 ) ) begin
            
                if ( iCommandFlag == 1 ) begin
                    case ( iDataRegister[7:6] ) 

                        // ECSS-E-ST-50-12C 8.5.3.2 gotNULL
                        // ECSS-E-ST-50-12C 8.5.3.3 gotFCT
                        2'b00: begin // FCT Receive or Null Receive
                            if ( iESCFlag == 1 ) begin
                                iReceiveNullOut = 1;
                                iESCFlag        = 0;
                            end
                            else begin
                                iReceiveFCTOut  = 1;
                            end
                        end

                        2'b11: begin // ESC Receive
                            if ( iESCFlag == 1 ) begin
                                iEscapeErrorOut = 1;
                            end
                            else begin
                                iESCFlag = 1;
                            end
                        end

                        2'b10: begin // EOP Receive
                            if ( iESCFlag == 1 ) begin
                                iEscapeErrorOut = 1;
                            end
                            else begin
                                iReceiverEOPOut = 1;
                            end
                        end

                        2'b01: begin // EEP Receive
                            if ( iESCFlag == 1 ) begin
                                iEscapeErrorOut = 1;
                            end
                            else begin
                                iReceiverEEPOut = 1;
                            end
                        end
                    endcase
                end         
                
                // ECSS-E-ST-50-12C 8.5.3.5 gotTime-Code
                // ECSS-E-ST-50-12C 8.5.3.4 gotN-Char
                else if ( iDataFlag == 1 ) begin
                    if ( iESCFlag == 1 ) begin // TimeCode Receive
                        iReceiveTimeCodeValidOut = 1;
                        iESCFlag = 0;
                    end
                    else begin
                        iReceiverDataValidOut = 1;
                    end
                end
            end

            // Clear the previous Receive flag before receiving data
            else if ( iBitCount == 1 && 
                      ( spaceWireState == spaceWireOdd0 || 
                        spaceWireState == spaceWireOdd1 ) ) begin

                iReceiverDataValidOut    = 0;
                iReceiveTimeCodeValidOut = 0;
                iReceiveNullOut          = 0;
                iReceiveFCTOut           = 0;
                iReceiverEOPOut          = 0;
                iReceiverEEPOut          = 0;
            end

            else if ( spaceWireState == spaceWireIdel ) begin
                iReceiverDataValidOut    = 0;
                iReceiveTimeCodeValidOut = 0;
                iReceiveNullOut          = 0;
                iReceiveFCTOut           = 0;
                iReceiverEOPOut          = 0;
                iReceiverEEPOut          = 0;
                iEscapeErrorOut          = 0;
                iESCFlag                 = 0;
            end
        end
        else begin
            iReceiverDataValidOut    = 0;
            iReceiveTimeCodeValidOut = 0;
            iReceiveNullOut          = 0;
            iReceiveFCTOut           = 0;
            iReceiverEOPOut          = 0;
            iReceiverEEPOut          = 0;
            iEscapeErrorOut          = 0;
            iESCFlag                 = 0;
        end
    end

endmodule