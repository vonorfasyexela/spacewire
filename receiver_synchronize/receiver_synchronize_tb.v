`timescale 1ns / 1ps

module receiver_synchronize_tb (

        output reg spaceWireStrobeIn,
        output reg spaceWireDataIn,

        input [8:0] receiveDataOut,
        input receiveDataValidOut,
        input [7:0] receiveTimeCodeOut,
        input receiveTimeCodeValidOut,
        input receiveNCharacterOut,
        input receiveFCTOut,
        input receiveNullOut,
        input receiveEEPOut,
        input receiveEOPOut,
        input receiveOffOut,
        input receiverErrorOut,
        input parityErrorOut,
        input escapeErrorOut,
        input disconnectErrorOut,

        output reg spaceWireReset,

        input receiveFIFOWriteEnable,

        output reg enableReceive,
        output reg receiveClock
    );

    receiver_synchronize _RECEIVER_SYNCHRONIZE (

        .spaceWireStrobeIn( spaceWireStrobeIn ),
        .spaceWireDataIn( spaceWireDataIn ),

        .receiveDataOut( receiveDataOut ),
        .receiveDataValidOut( receiveDataValidOut ),
        .receiveTimeCodeOut( receiveTimeCodeOut ),
        .receiveTimeCodeValidOut( receiveTimeCodeValidOut ),
        .receiveNCharacterOut( receiveNCharacterOut ),
        .receiveFCTOut( receiveFCTOut ),
        .receiveNullOut( receiveNullOut ),
        .receiveEEPOut( receiveEEPOut ),
        .receiveEOPOut( receiveEOPOut ),
        .receiveOffOut( receiveOffOut ),
        .receiverErrorOut( receiverErrorOut ),
        .parityErrorOut( parityErrorOut ),
        .escapeErrorOut( escapeErrorOut ),
        .disconnectErrorOut( disconnectErrorOut ),

        .spaceWireReset( spaceWireReset ),

        .receiveFIFOWriteEnable( receiveFIFOWriteEnable ),

        .enableReceive( enableReceive ),
        .receiveClock( receiveClock )
    );

    initial begin
        $dumpfile( "receiver_synchronize_tb.vcd" );
        $dumpvars( 0, receiver_synchronize_tb );

        #3000 $finish;
    end



endmodule