/*
    Данный модуль реализует структуру данных типа очередь.
    Глубина очереди 64 элемента, разрядность данных - 9 бит.
*/
module fifo (

        // сброс модуля
        input reset,

        // данные, которые будут записаны в очередь
        input [8:0] writeDataIn,
        // по каждому такту сигнала
        input writeClock,
        // в случае, если активен сигнал
        input writeEnable,
        // при этом будет увеличено значение
        output [5:0] writeDataCount,
        
        // данные, которые будут вычитаны из очереди
        output [8:0] readDataOut,
        // по каждому такту сигнала 
        input readClock,
        // в случае, если активен сигнал
        input readEnable,
        // при этом будет увеличено значение
        output [5:0] readDataCount,
        
        // флаги состояний "очередь пуста" и "очередь заполнена"
        output empty,
        output full

    );

    wire iFull;
    wire iEmpty;
    reg [8:0] iReadDataOut;
    
    /*
        Сброс модуля        
    */
    reg iWriteReset;
    reg [1:0] iWriteResetTime;

    always @( posedge writeClock or posedge reset ) begin
        if ( reset == 1 ) begin
            iWriteResetTime <= 2'b11;
            iWriteReset     <= 1;
        end
        else begin
            iWriteResetTime <= { iWriteResetTime[0], reset };
            iWriteReset     <= iWriteResetTime[1];
        end
    end    

    reg iReadReset;
    reg [1:0] iReadResetTime;

    always @( posedge readClock or posedge reset ) begin
        if ( reset == 1 ) begin
            iReadResetTime <= 2'b11;
            iReadReset     <= 1;
        end
        else begin
            iReadResetTime <= { iReadResetTime[0], reset };
            iReadReset     <= iReadResetTime[1];
        end
    end    

    /*
        Для непосредственного хранения данных используется регистровая 
        переменная dpram, которая представляет собой массив из 64 9-разрядных 
        регистров
    */
    reg [8:0] dpram [0:63];

    /*
        Очередь имеет два указателя - куда конкретно писать очередные 
        данные ("голову") ... 
    */
    reg [5:0] iWritePointer;

    // запись данных по текущему значению указателя "головы"
    always @( posedge writeClock ) begin
        if ( writeEnable == 1 ) begin
            dpram[iWritePointer] <= writeDataIn;            
        end
    end

    // увеличиваем также размер очереди на 1
    always @( posedge writeClock ) begin
        if ( iWriteReset == 1 ) begin
            iWritePointer <= 0;
        end
        else if ( writeEnable == 1 ) begin
            iWritePointer <= iWritePointer + 1;
        end
    end

    /*
        ... и откуда читать данные ("хвост")
    */
    reg [5:0] iReadPointer;

    // чтение данных по текущему значению указателя "хвоста"
    always @( posedge readClock ) begin
        if ( iEmpty == 0 ) begin
            if ( readEnable == 1 ) begin
                iReadDataOut <= dpram[iReadPointer];
            end
        end
    end

    // уменьшаем размер очереди на 1
    always @( posedge readClock ) begin
        if ( iReadReset == 1 ) begin
            iReadPointer <= 0;
        end
        else if ( iEmpty == 0 ) begin
            if ( readEnable == 1 ) begin
                iReadPointer <= iReadPointer + 1;
            end
        end
    end

    /*
        Для выявления значений флагов необходимо выполнить следующие проверки:

        - очередь пуста, когда значение указателей "головы" и "хвоста" равны
        - количество хранимых элементов равно разности указателей "головы" и 
            "хвоста"
        - очередь полна, когда число хранимых элементов на единицу меньше 
            глубины очереди

        Вот здесь возникает проблема. Синхросигналы writeClock и readClock 
        являются асинхронными по отношению друг к другу, и могут иметь 
        разную частоту и разность фаз. Поэтому мы пока не можем сравнивать 
        между собой указатели "головы" и "хвоста".

        Эта проблема получила название "пересечение домена синхрочастоты" 
        (CDC, clock domain crossing). Для ее решения применяется код Грея. 
        Он представляет собой последовательность чисел, в которой соседние 
        числа отличаются на один разряд. 
    */
    
    // таблица перевода двоичных чисел в коды Грея
    wire [5:0] binaryToGray [0:63];
    assign binaryToGray[0]  = 6'b000000;
    assign binaryToGray[1]  = 6'b000001; 
    assign binaryToGray[2]  = 6'b000011;
    assign binaryToGray[3]  = 6'b000010;
    assign binaryToGray[4]  = 6'b000110;
    assign binaryToGray[5]  = 6'b000111; 
    assign binaryToGray[6]  = 6'b000101; 
    assign binaryToGray[7]  = 6'b000100;
    assign binaryToGray[8]  = 6'b001100; 
    assign binaryToGray[9]  = 6'b001101; 
    assign binaryToGray[10] = 6'b001111; 
    assign binaryToGray[11] = 6'b001110;
    assign binaryToGray[12] = 6'b001010; 
    assign binaryToGray[13] = 6'b001011; 
    assign binaryToGray[14] = 6'b001001; 
    assign binaryToGray[15] = 6'b001000;
    assign binaryToGray[16] = 6'b011000;
    assign binaryToGray[17] = 6'b011001;
    assign binaryToGray[18] = 6'b011011; 
    assign binaryToGray[19] = 6'b011010;
    assign binaryToGray[20] = 6'b011110; 
    assign binaryToGray[21] = 6'b011111; 
    assign binaryToGray[22] = 6'b011101; 
    assign binaryToGray[23] = 6'b011100;
    assign binaryToGray[24] = 6'b010100; 
    assign binaryToGray[25] = 6'b010101; 
    assign binaryToGray[26] = 6'b010111; 
    assign binaryToGray[27] = 6'b010110;
    assign binaryToGray[28] = 6'b010010; 
    assign binaryToGray[29] = 6'b010011; 
    assign binaryToGray[30] = 6'b010001; 
    assign binaryToGray[31] = 6'b010000;
    assign binaryToGray[32] = 6'b110000; 
    assign binaryToGray[33] = 6'b110001; 
    assign binaryToGray[34] = 6'b110011; 
    assign binaryToGray[35] = 6'b110010;
    assign binaryToGray[36] = 6'b110110; 
    assign binaryToGray[37] = 6'b110111; 
    assign binaryToGray[38] = 6'b110101; 
    assign binaryToGray[39] = 6'b110100;
    assign binaryToGray[40] = 6'b111100; 
    assign binaryToGray[41] = 6'b111101; 
    assign binaryToGray[42] = 6'b111111; 
    assign binaryToGray[43] = 6'b111110;
    assign binaryToGray[44] = 6'b111010; 
    assign binaryToGray[45] = 6'b111011; 
    assign binaryToGray[46] = 6'b111001; 
    assign binaryToGray[47] = 6'b111000;
    assign binaryToGray[48] = 6'b101000; 
    assign binaryToGray[49] = 6'b101001; 
    assign binaryToGray[50] = 6'b101011; 
    assign binaryToGray[51] = 6'b101010;
    assign binaryToGray[52] = 6'b101110; 
    assign binaryToGray[53] = 6'b101111; 
    assign binaryToGray[54] = 6'b101101; 
    assign binaryToGray[55] = 6'b101100;
    assign binaryToGray[56] = 6'b100100; 
    assign binaryToGray[57] = 6'b100101; 
    assign binaryToGray[58] = 6'b100111; 
    assign binaryToGray[59] = 6'b100110;
    assign binaryToGray[60] = 6'b100010; 
    assign binaryToGray[61] = 6'b100011; 
    assign binaryToGray[62] = 6'b100001; 
    assign binaryToGray[63] = 6'b100000;

    // и таблица для обратной операции
    wire [5:0] grayToBinary [0:63];
    assign grayToBinary[0]  = 6'b000000; 
    assign grayToBinary[1]  = 6'b000001; 
    assign grayToBinary[2]  = 6'b000011; 
    assign grayToBinary[3]  = 6'b000010;
    assign grayToBinary[4]  = 6'b000111; 
    assign grayToBinary[5]  = 6'b000110; 
    assign grayToBinary[6]  = 6'b000100; 
    assign grayToBinary[7]  = 6'b000101;
    assign grayToBinary[8]  = 6'b001111; 
    assign grayToBinary[9]  = 6'b001110; 
    assign grayToBinary[10] = 6'b001100; 
    assign grayToBinary[11] = 6'b001101;
    assign grayToBinary[12] = 6'b001000; 
    assign grayToBinary[13] = 6'b001001; 
    assign grayToBinary[14] = 6'b001011; 
    assign grayToBinary[15] = 6'b001010;
    assign grayToBinary[16] = 6'b011111; 
    assign grayToBinary[17] = 6'b011110; 
    assign grayToBinary[18] = 6'b011100; 
    assign grayToBinary[19] = 6'b011101;
    assign grayToBinary[20] = 6'b011000; 
    assign grayToBinary[21] = 6'b011001; 
    assign grayToBinary[22] = 6'b011011; 
    assign grayToBinary[23] = 6'b011010;
    assign grayToBinary[24] = 6'b010000; 
    assign grayToBinary[25] = 6'b010001; 
    assign grayToBinary[26] = 6'b010011; 
    assign grayToBinary[27] = 6'b010010;
    assign grayToBinary[28] = 6'b010111; 
    assign grayToBinary[29] = 6'b010110; 
    assign grayToBinary[30] = 6'b010100; 
    assign grayToBinary[31] = 6'b010101;
    assign grayToBinary[32] = 6'b111111; 
    assign grayToBinary[33] = 6'b111110; 
    assign grayToBinary[34] = 6'b111100; 
    assign grayToBinary[35] = 6'b111101;
    assign grayToBinary[36] = 6'b111000; 
    assign grayToBinary[37] = 6'b111001; 
    assign grayToBinary[38] = 6'b111011; 
    assign grayToBinary[39] = 6'b111010;
    assign grayToBinary[40] = 6'b110000; 
    assign grayToBinary[41] = 6'b110001; 
    assign grayToBinary[42] = 6'b110011; 
    assign grayToBinary[43] = 6'b110010;
    assign grayToBinary[44] = 6'b110111; 
    assign grayToBinary[45] = 6'b110110; 
    assign grayToBinary[46] = 6'b110100; 
    assign grayToBinary[47] = 6'b110101;
    assign grayToBinary[48] = 6'b100000; 
    assign grayToBinary[49] = 6'b100001; 
    assign grayToBinary[50] = 6'b100011; 
    assign grayToBinary[51] = 6'b100010;
    assign grayToBinary[52] = 6'b100111; 
    assign grayToBinary[53] = 6'b100110; 
    assign grayToBinary[54] = 6'b100100; 
    assign grayToBinary[55] = 6'b100101;
    assign grayToBinary[56] = 6'b101111; 
    assign grayToBinary[57] = 6'b101110; 
    assign grayToBinary[58] = 6'b101100; 
    assign grayToBinary[59] = 6'b101101;
    assign grayToBinary[60] = 6'b101000; 
    assign grayToBinary[61] = 6'b101001; 
    assign grayToBinary[62] = 6'b101011; 
    assign grayToBinary[63] = 6'b101010;

    /*
        На каждом такте синхрочастот writeClock и readClock мы переводим
        бинарные значения указателей "головы" и "хвоста" в коды Грея...
    */

    reg [5:0] iGrayWritePointer;

    always @( posedge writeClock ) begin
        if ( iWriteReset == 1 ) begin
            iGrayWritePointer <= 0;
        end
        else begin
            iGrayWritePointer <= binaryToGray[iWritePointer];
        end
    end

    reg [5:0] iGrayReadPointer;

    always @( readClock ) begin
        if ( iReadReset == 1 ) begin
            iGrayReadPointer <= 0;
        end
        else begin
            iGrayReadPointer <= binaryToGray[iReadPointer];
        end
    end

    /*
        ... а затем пропускаем значения указателей через группу 
        сихронизаторов (несколько триггеров, соединенных последовательно). 
        После перевода значений обратно в бинарную форму мы получаем 
        значения указателей в одном домене синхронизации
    */

    reg [5:0] iGrayWritePointer1;
    reg [5:0] iGrayWritePointer2;
    reg [5:0] iGrayWritePointer3;
    reg [5:0] iWritePointer4;

    always @( posedge readClock ) begin
        if ( iReadReset == 1 ) begin
            iGrayWritePointer1 <= 0;
            iGrayWritePointer2 <= 0;
            iGrayWritePointer3 <= 0;
            iWritePointer4 <= 0;
        end
        else begin
            // синхронизаторы
            iGrayWritePointer1 <= iGrayWritePointer;
            iGrayWritePointer2 <= iGrayWritePointer1;
            iGrayWritePointer3 <= iGrayWritePointer2;
            iWritePointer4     <= grayToBinary[iGrayWritePointer3];
        end
    end

    reg [5:0] iGrayReadPointer1;
    reg [5:0] iGrayReadPointer2;
    reg [5:0] iReadPointer3;

    always @( posedge writeClock ) begin
        if ( iWriteReset == 1 ) begin
            iGrayReadPointer1 <= 6'b000000;
            iGrayReadPointer2 <= 6'b000000;
            iReadPointer3     <= 6'b000000;
        end
        else begin
            // синхронизаторы
            iGrayReadPointer1 <= iGrayReadPointer;
            iGrayReadPointer2 <= iGrayReadPointer1;
            iReadPointer3     <= grayToBinary[iGrayReadPointer2];
        end
    end

    /*
        Теперь мы имеем полное право сравнивать указатели и вычислять 
        значения флагов.
    */
    
    assign full           = iFull;
    
    // здесь 6'b111000 - это количество элементов в очереди (56), после 
    // которого она считается полной
    
    assign iFull = ( ( (iWritePointer - iReadPointer3) > 6'b111000 ) || 
                      iWriteReset == 1 ) ? 1 : 0;

    
    assign empty          = iEmpty;
    assign iEmpty = ( iWritePointer4 == iReadPointer || iReadReset == 1 ) ? 1 : 0;

    
    assign readDataOut    = iReadDataOut;

    wire [5:0] iWriteDataCount;
    wire [5:0] iReadDataCount;

    assign writeDataCount = iWriteDataCount;
    assign readDataCount  = iReadDataCount;
    assign iWriteDataCount = iWritePointer - iReadPointer3;
    assign iReadDataCount = iWritePointer4 - iReadPointer;

endmodule