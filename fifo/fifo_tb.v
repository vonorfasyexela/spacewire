`timescale 1ns / 1ps

module fifo_tb (

        output reg [8:0] writeDataIn,
        output reg readClock,
        output reg readEnable,
        output reg reset,
        output reg writeClock,
        output reg writeEnable,

        input [8:0] readDataOut,
        input empty,
        input full,
        input [5:0] readDataCount,
        input [5:0] writeDataCount

    );

    integer i;

    // #     # #     # #######
    // #     # #     #    #
    // #     # #     #    #
    // #     # #     #    #
    // #     # #     #    #
    // #     # #     #    #
    //  #####   #####     #

    fifo _FIFO (

        .writeDataIn( writeDataIn ),
        .readClock( readClock ),
        .readEnable( readEnable ),
        .reset( reset ),
        .writeClock( writeClock ),
        .writeEnable( writeEnable ),
        
        .readDataOut( readDataOut ),
        .empty( empty ),
        .full( full ),
        .readDataCount( readDataCount ),
        .writeDataCount( writeDataCount )

    );

    // #######    #     #####  #    #  #####
    //    #      # #   #     # #   #  #     #
    //    #     #   #  #       #  #   #
    //    #    #     #  #####  ###     #####
    //    #    #######       # #  #         #
    //    #    #     # #     # #   #  #     #
    //    #    #     #  #####  #    #  #####

    task write_data( input reg [8:0] data );
        begin
            writeDataIn = data;
            #2;
            writeEnable = 1;
            #10;
            writeEnable = 0;
        end
    endtask

    task read_data;
        begin
            readEnable = 1;
            #10;
            readEnable = 0;
            #2;
        end
    endtask


    initial begin
        // для правильной работы симулятора iVerilog Icarus
        // по идее в Active-HDL эти две строки не нужны
        $dumpfile( "fifo_tb.vcd" );
        $dumpvars( 0, fifo_tb );

        #10000 $finish;
    end

    initial begin

        // старт
        reset = 1;
        writeDataIn = 0;
        writeClock = 0;
        writeEnable = 0;
    
        readClock = 0;
        readEnable = 0;        

        // выключаем сброс
        #10 reset = 0;
        #50;

        // пишем произвольные 9 бит
        write_data( 9'h17A );

        #100;

        // читаем
        read_data;

        #200;

        // заполняем очередь полностью
        for (i = 0; i < 63; i = i + 1)
            begin
                write_data( i );
                #15;
            end  

        #200;

        // полностью вычитываем данные
        #1000;
        for (i = 0; i < 63; i = i + 1)
            begin
                read_data;
                #15;
            end  

        #1000;

        // пишем три слова
        write_data( 9'h17B );
        #50;
        write_data( 9'h03F );
        #50;
        write_data( 9'h118 );
        #50;

        // читаем три слова
        read_data;
        #50;
        read_data;
        #50;
        read_data;
        #50;

    end

    always 
        #5 writeClock = !writeClock;

    always 
        #5 readClock = !readClock;

endmodule