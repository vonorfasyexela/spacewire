`timescale 1ns / 1ps

/*
    • Cочетание символов ESC и FCT образует NULL-код, который всегда 
    транслируется, если в канале не передаются символы данных и управления 
    (это позволяет сохранять активность канала и определить ошибку 
    рассоединения)

    • Узел-абонент принципиально отличается от коммутатора тем, что трансляция 
    данных между его линк-портами, при необходимости, возможна только под 
    управлением хост-устройства (т.е. реализуется программно), а трансляция 
    управляющих кодов (например, маркеров времени) не производится. Напротив, 
    маршрутизирующий коммутатор обеспечивает непосредственную передачу трафика 
    между своими входными и выходными портами

    • В сетях SpaceWire могут образовываться и структуры из одних 
    узлов-абонентов с прямыми связями между ними, без коммутаторов

    • Формат пакета с адресом места назначения ориентирован на передачу пакета 
    через сеть из маршрутизаторов. Именно они в основном и обрабатывают 
    заголовок с адресом назначения пакета. Если же пакет передается между 
    двумя абонентами, связанными прямым каналом (точка-точка), то поле адреса 
    назначения может быть и пустым

    • Полезная нагрузка – это данные, т.е. информация, передаваемая от 
    источника к узлу назначения. Закрывает пакет маркер конца пакета, указывая 
    также, является ли пакет правильным (EOP, End of Packet) или транслируется 
    с уже обнаруженной ошибкой (EEP, Error End of Packet)

    • После того, как соединение установлено, оно поддерживается постоянной 
    передачей по нему символов данных, управляющих кодов, а при их 
    отсутствии – кодов NULL. Если сигналы в линке не изменяются в течение 
    850 нс, это рассматривается как разрыв соединения (отсюда – и требование 
    на минимальную скорость передачи 2 Мбит/с)

    • Предусмотрена возможность запуска соединения по инициативе одного из 
    абонентов (режим Autostart): по получении кода NULL второй абонент 
    автоматически, без команды со стороны своей хост-системы, стартует и 
    входит в соединение.

    • При разрыве соединения абоненты автоматически выходят на новое 
    установление соединения, после чего передача информации продолжается.

    • При DS-кодировании данные передаются по линии данных (D) в прямом коде, 
    в то время как сигнал на линии строба (S) изменяет свое значение каждый 
    раз, когда данные остаются неизменными в очередном битовом интервале. 
    Таким образом, в каждом такте меняется один из сигналов – либо D, либо S.

    • DS-кодирование обладает свойством самосинхронизации: в DS-сигналах 
    закодирован синхросигнал, который восстанавливается на приемной стороне 
    элементарной операцией "исключающего ИЛИ" (XOR) над сигналами D и S.

    • DS-кодирование много проще и компактней в схемотехнической реализации, 
    чем, например, распространенное кодирование 8В/10В. Это обеспечивает одно 
    из важных преимуществ DS-кодирования – компактную реализацию DS-кодека в 
    СБИС и малое энергопотребление. 

    • В стандарте SpaceWire используются дуплексные соединения типа 
    точка-точка. S- и D-сигналы передаются каждый по отдельной 
    дифференциальной паре. Линии передачи – однонаправленные, поэтому всего в 
    линке SpaceWire четыре дифференциальные пары, по две для каждого 
    направления.

    • Стандарт ECSS-E-50-12A регламентирует скорости передачи по каналу 
    SpaceWire от 2 до 400 Мбит/с на расстояния до 10 м.

    • В лабораторных условиях при благоприятной помеховой обстановке удается 
    обеспечивать нормальную работу со скоростями более 300 Мбит/с даже на 
    обычном кабеле UTP 5e. 

    • Пользователь может выбирать и какие-либо существующие протоколы 
    транспортного уровня (например, TCP, UDP и др.) для реализации поверх 
    коммуникационной сети SpaceWire. 

*/
module spacewire_tb;

    // Inputs
    reg clock;
    reg transmitClock;
    reg receiveClock;
    reg reset;
    reg transmitFIFOWriteEnable;
    reg [8:0] transmitFIFODataIn;
    reg receiveFIFOReadEnable;
    reg tickIn;
    reg [5:0] timeIn;
    reg [1:0] controlFlagsIn;
    reg linkStart;
    reg linkDisable;
    reg autoStart;
    reg [5:0] transmitClockDivideValue;
    reg spaceWireDataIn;
    reg spaceWireStrobeIn;
    reg statisticalInformationClear;

    // Outputs
    wire transmitFIFOFull;
    wire [5:0] transmitFIFODataCount;
    wire [8:0] receiveFIFODataOut;
    wire receiveFIFOFull;
    wire receiveFIFOEmpty;
    wire [5:0] receiveFIFODataCount;
    wire tickOut;
    wire [5:0] timeOut;
    wire [1:0] controlFlagsOut;
    wire [15:0] linkStatus;
    wire [7:0] errorStatus;
    wire [5:0] creditCount;
    wire [5:0] outstandingCount;
    wire transmitActivity;
    wire receiveActivity;
    wire spaceWireDataOut;
    wire spaceWireStrobeOut;
    //wire [7:0] statisticalInformation;

    spacewire uut_verilog (
        .clock(clock), 
        .transmitClock(transmitClock), 
        .receiveClock(receiveClock), 
        .reset(reset), 
        .transmitFIFOWriteEnable(transmitFIFOWriteEnable), 
        .transmitFIFODataIn(transmitFIFODataIn), 
        .transmitFIFOFull(transmitFIFOFull), 
        .transmitFIFODataCount(transmitFIFODataCount), 
        .receiveFIFOReadEnable(receiveFIFOReadEnable), 
        .receiveFIFODataOut(receiveFIFODataOut), 
        .receiveFIFOFull(receiveFIFOFull), 
        .receiveFIFOEmpty(receiveFIFOEmpty), 
        .receiveFIFODataCount(receiveFIFODataCount), 
        .tickIn(tickIn), 
        .timeIn(timeIn), 
        .controlFlagsIn(controlFlagsIn), 
        .tickOut(tickOut), 
        .timeOut(timeOut), 
        .controlFlagsOut(controlFlagsOut), 
        .linkStart(linkStart), 
        .linkDisable(linkDisable), 
        .autoStart(autoStart), 
        .linkStatus(linkStatus), 
        .errorStatus(errorStatus), 
        .transmitClockDivideValue(transmitClockDivideValue), 
        .creditCount(creditCount), 
        .outstandingCount(outstandingCount), 
        .transmitActivity(transmitActivity), 
        .receiveActivity(receiveActivity), 
        .spaceWireDataOut(spaceWireDataOut), 
        .spaceWireStrobeOut(spaceWireStrobeOut), 
        .spaceWireDataIn(spaceWireDataIn), 
        .spaceWireStrobeIn(spaceWireStrobeIn), 
        .statisticalInformationClear(statisticalInformationClear)
        //.statisticalInformation(statisticalInformation)
    );

    SpaceWireCODECIP uut (
        .clock(clock), 
        .transmitClock(transmitClock), 
        .receiveClock(receiveClock), 
        .reset(reset), 
        .transmitFIFOWriteEnable(transmitFIFOWriteEnable), 
        .transmitFIFODataIn(transmitFIFODataIn), 
        .transmitFIFOFull(transmitFIFOFull), 
        .transmitFIFODataCount(transmitFIFODataCount), 
        .receiveFIFOReadEnable(receiveFIFOReadEnable), 
        .receiveFIFODataOut(receiveFIFODataOut), 
        .receiveFIFOFull(receiveFIFOFull), 
        .receiveFIFOEmpty(receiveFIFOEmpty), 
        .receiveFIFODataCount(receiveFIFODataCount), 
        .tickIn(tickIn), 
        .timeIn(timeIn), 
        .controlFlagsIn(controlFlagsIn), 
        .tickOut(tickOut), 
        .timeOut(timeOut), 
        .controlFlagsOut(controlFlagsOut), 
        .linkStart(linkStart), 
        .linkDisable(linkDisable), 
        .autoStart(autoStart), 
        .linkStatus(linkStatus), 
        .errorStatus(errorStatus), 
        .transmitClockDivideValue(transmitClockDivideValue), 
        .creditCount(creditCount), 
        .outstandingCount(outstandingCount), 
        .transmitActivity(transmitActivity), 
        .receiveActivity(receiveActivity), 
        .spaceWireDataOut(spaceWireDataOut), 
        .spaceWireStrobeOut(spaceWireStrobeOut), 
        .spaceWireDataIn(spaceWireDataIn), 
        .spaceWireStrobeIn(spaceWireStrobeIn), 
        .statisticalInformationClear(statisticalInformationClear)
        //.statisticalInformation(statisticalInformation)
    );
    
    // tasks
    task write_data( input reg [8:0] data );
        begin
            transmitFIFODataIn = data;
            #2;
            transmitFIFOWriteEnable = 1;
            #10;
            transmitFIFOWriteEnable = 0;
        end
    endtask


    initial begin
        // Initialize Inputs
        clock = 0;
        transmitClock = 0;
        receiveClock = 0;
        reset = 1;
        transmitFIFOWriteEnable = 0;
        transmitFIFODataIn = 0;
        receiveFIFOReadEnable = 0;
        tickIn = 0;
        timeIn = 0;
        controlFlagsIn = 0;
        linkStart = 0;
        linkDisable = 0;
        autoStart = 1;
        spaceWireDataIn = 0;
        spaceWireStrobeIn = 0;
        statisticalInformationClear = 0;

        // Wait 100 ns for global reset to finish
        #100;
        
        // Add stimulus here
        reset = 0;
        
        #100;
        
        // write data
        //write_data( 9'h17A );

		
        #20000;
        
        linkStart = 1;
        #20;
        linkStart = 0;

    end
    
    always 
        #5 clock = !clock;
    
    always 
        #5 transmitClock = !transmitClock;
        
    always 
        #5 receiveClock = !receiveClock;
     
endmodule

