module time_code_control (

        input clock,
        input reset,
        input receiveClock,
        input gotTimeCode,
        input [7:0] receiveTimeCodeOut,

        output [5:0] timeOut,
        output [1:0] controlFlagsOut,
        output tickOut
    );

    // signals
    reg [7:0] iReceiveTimeCodeOutRegister;
    reg [1:0] iControlFlags;
    reg [5:0] iReceiveTimeCode;
    reg [5:0] iReceiveTimeCodePlus1;
    reg iTickOutSignal;
    wire gotTimeCodeSynchronized;

    assign timeOut         = iReceiveTimeCode;
    assign controlFlagsOut = iControlFlags;
    assign tickOut         = iTickOutSignal;

    // synchronizer
    synchronize_one_pulse _TIME_CODE_PULSE (
        .clock( clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( gotTimeCode ),

        .synchronizedOut( gotTimeCodeSynchronized )
    );

    // ECSS-E-ST-50-12C 8.12 System time distribution (normative)
    // ECSS-E-ST-50-12C 7.3 Control characters and control codes
    // The new time should be one more than the time-counter's previous
    // time-value.
    always @( posedge clock or posedge reset ) begin
        if ( reset == 1 ) begin
            iReceiveTimeCode            = 0;
            iReceiveTimeCodePlus1       = 6'b000001;
            iTickOutSignal              = 0;
            iControlFlags               = 2'b00;
            iReceiveTimeCodeOutRegister = 0;
        end
        else begin
            if ( gotTimeCodeSynchronized == 1 ) begin
                iControlFlags         = iReceiveTimeCodeOutRegister[7:6];
                iReceiveTimeCode      = iReceiveTimeCodeOutRegister[5:0];
                iReceiveTimeCodePlus1 = iReceiveTimeCodeOutRegister[5:0] + 1;
                if ( iReceiveTimeCodePlus1 == 
                        iReceiveTimeCodeOutRegister[5:0] ) begin
                    iTickOutSignal = 1;
                end
            end
            else begin
                iTickOutSignal = 0;
            end
            iReceiveTimeCodeOutRegister = receiveTimeCodeOut;
        end
    end

endmodule