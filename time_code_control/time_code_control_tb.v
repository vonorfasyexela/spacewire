`timescale 1ns / 1ps

module time_code_control_tb (

        output reg clock,
        output reg reset,
        output reg receiveClock,
        output reg gotTimeCode,
        output reg [7:0] receiveTimeCodeOut,

        input [5:0] timeOut,
        input [1:0] controlFlagsOut,
        input tickOut

    );

    time_code_control _TIME_CODE_CONTROL (

        .clock( clock ),
        .reset( reset ),
        .receiveClock( receiveClock ),
        .gotTimeCode( gotTimeCode ),
        .receiveTimeCodeOut( receiveTimeCodeOut ),

        .timeOut( timeOut ),
        .controlFlagsOut( controlFlagsOut ),
        .tickOut( tickOut )

    );

    initial begin
        $dumpfile( "time_code_control_tb.vcd" );
        $dumpvars( 0, time_code_control_tb );

        clock = 0;
        reset = 1;

        #10 reset = 0;
    end

    initial begin
        #3000 $finish;
    end

    always 
        #5 clock = !clock;

endmodule