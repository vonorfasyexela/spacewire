module statistical_information_count (

        input clock,
        input reset,
        input statisticalInformationClear,
        input transmitClock,
        input receiveClock,
        //
        input receiveEEPAsynchronous,
        input receiveEOPAsynchronous,
        input receiveByteAsynchronous,
        // 
        input transmitEEPAsynchronous,
        input transmitEOPAsynchronous,
        input transmitByteAsynchronous,
        // 
        input linkUpTransition,
        input linkDownTransition,
        input linkUpEnable,
        // 
        input nullSynchronous,
        input fctSynchronous,
        // 
        // output [31:0] statisticalInformation[7:0],
        output [31:0] statisticalInformation0,
        output [31:0] statisticalInformation1,
        output [31:0] statisticalInformation2,
        output [31:0] statisticalInformation3,
        output [31:0] statisticalInformation4,
        output [31:0] statisticalInformation5,
        output [31:0] statisticalInformation6,
        output [31:0] statisticalInformation7,

        output [6:0] characterMonitor

    );

    // signals
    reg [31:0] iTransmitEOPCount;
    reg [31:0] iReceiveEOPCount;
    reg [31:0] iTransmitEEPCount;
    reg [31:0] iReceiveEEPCount;
    reg [31:0] iTransmitByteCount;
    reg [31:0] iReceiveByteCount;
    reg [31:0] iLinkUpCount;
    reg [31:0] iLinkDownCount;
    // 
    reg [6:0] iCharacterMonitor;
    // 
    wire iReceiveEEPSynchronize;
    wire iReceiveEOPSynchronize;
    wire iReceiveByteSynchronize;
    wire iTransmitEEPSynchronize;
    wire iTransmitEOPSynchronize;
    wire iTransmitByteSynchronize;


    assign characterMonitor        = iCharacterMonitor;
    assign statisticalInformation0 = iTransmitEOPCount;
    assign statisticalInformation1 = iReceiveEOPCount;
    assign statisticalInformation2 = iTransmitEEPCount;
    assign statisticalInformation3 = iReceiveEEPCount;
    assign statisticalInformation4 = iTransmitByteCount;
    assign statisticalInformation5 = iReceiveByteCount;
    assign statisticalInformation6 = iLinkUpCount;
    assign statisticalInformation7 = iLinkDownCount;

    // synchronizers
    synchronize_one_pulse _RECEIVE_EEP_PULSE (

        .clock( clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( receiveEEPAsynchronous ),

        .synchronizedOut( iReceiveEEPSynchronize )

    );

    synchronize_one_pulse _RECEIVE_EOP_PULSE (

        .clock( clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( receiveEOPAsynchronous ),

        .synchronizedOut( iReceiveEOPSynchronize )

    );

    synchronize_one_pulse _RECEIVE_BYTE_PULSE (

        .clock( clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( receiveByteAsynchronous ),

        .synchronizedOut( iReceiveByteSynchronize )

    );

    synchronize_one_pulse _TRANSMIT_EEP_PULSE (

        .clock( clock ),
        .asynchronousClock( transmitClock ),
        .reset( reset ),
        .asynchronousIn( transmitEEPAsynchronous ),

        .synchronizedOut( iTransmitEEPSynchronize )

    );

    synchronize_one_pulse _TRANSMIT_EOP_PULSE (

        .clock( clock ),
        .asynchronousClock( transmitClock ),
        .reset( reset ),
        .asynchronousIn( transmitEOPAsynchronous ),

        .synchronizedOut( iTransmitEOPSynchronize )

    );

    synchronize_one_pulse _TRANSMIT_BYTE_PULSE (

        .clock( clock ),
        .asynchronousClock( transmitClock ),
        .reset( reset ),
        .asynchronousIn( transmitByteAsynchronous ),

        .synchronizedOut( iTransmitByteSynchronize )

    );


    // One Shot Status Information
    // always @( clock or reset ) begin
    always @( posedge clock or posedge reset ) begin
        if ( reset == 1 ) begin
            iCharacterMonitor = 0;
        end
        else begin
            iCharacterMonitor = { iReceiveEEPSynchronize, 
                                  iReceiveEOPSynchronize, 
                                  fctSynchronous, 
                                  nullSynchronous, 
                                  iReceiveByteSynchronize, 
                                  iTransmitByteSynchronize };
        end
    end

    // Statistical Information Counter.
    // Transmit and Receive EOP, EEP, 1Byte, SpaceWireLinkUP and 
    // SpaceWireLinkDown 
    // Increment Counter.
    // Status Information
    // Receive EOP, EEP, FCT, Null and 1Byte One Shot Pulse
    // Transmit 1Byte One Shot Pulse.

    // Transmit EOP, EEP and 1Byte Increment Counter
    always @( posedge clock or 
              posedge reset or 
              posedge statisticalInformationClear ) begin
        
        if ( reset == 1 ) begin
            iTransmitEOPCount  = 0;
            iTransmitEEPCount  = 0;
            iTransmitByteCount = 0;
        end
        else if ( statisticalInformationClear == 1 ) begin
            iTransmitEOPCount  = 0;
            iTransmitEEPCount  = 0;
            iTransmitByteCount = 0;
        end
        else begin
            if ( iTransmitEEPSynchronize == 1 ) begin
                iTransmitEEPCount = iTransmitEEPCount + 1;
            end
            if ( iTransmitEOPSynchronize == 1 ) begin
                iTransmitEOPCount = iTransmitEOPCount + 1;
            end
            if ( iTransmitByteSynchronize == 1 ) begin
                iTransmitByteCount = iTransmitByteCount + 1;
            end
        end
    end

    // receive EOP,EEP,1Byte Increment Counter
    always @( posedge clock or
              posedge reset or 
              posedge statisticalInformationClear ) begin
    
        if ( reset == 1 ) begin
            iReceiveEOPCount  = 0;
            iReceiveEEPCount  = 0;
            iReceiveByteCount = 0;
        end              
        else if ( statisticalInformationClear == 1 ) begin
            iReceiveEOPCount  = 0;
            iReceiveEEPCount  = 0;
            iReceiveByteCount = 0;
        end
        else begin
            if ( iReceiveEEPSynchronize == 1 ) begin
                iReceiveEEPCount = iReceiveEEPCount + 1;
            end
            if ( iReceiveEOPSynchronize == 1 ) begin
                iReceiveEOPCount = iReceiveEOPCount + 1;
            end
            if ( iReceiveByteSynchronize == 1 ) begin
                iReceiveByteCount = iReceiveByteCount + 1;
            end
        end
    end

    always @( posedge clock or 
              posedge reset or
              posedge statisticalInformationClear ) begin
    
        if ( reset == 1 ) begin
            iLinkUpCount   = 0;
            iLinkDownCount = 0;
        end
        else if ( statisticalInformationClear == 1 ) begin
            iLinkUpCount   = 0;
            iLinkDownCount = 0;
        end
        else begin
            if ( linkUpTransition == 1 ) begin
                iLinkUpCount = iLinkUpCount + 1;
            end
            if ( linkDownTransition == 1 ) begin
                iLinkDownCount = iLinkDownCount + 1;
            end
        end
    end

endmodule