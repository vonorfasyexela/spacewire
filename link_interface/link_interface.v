module link_interface #( 

        parameter gDisconnectCountValue = 141,
        gTimer6p4usValue                = 640,
        gTimer12p8usValue               = 1280,
        gTransmitClockDivideValue       = 6'b001001

    ) (

        input clock,
        input reset,

        // state machine
        input transmitClock,
        input linkStart,
        input linkDisable,
        input autoStart,
        output [15:0] linkStatus,
        output [7:0] errorStatus,
        output spaceWireResetOut,
        input FIFOAvailable,

        // transmitter
        input tickIn,
        input [5:0] timeIn,
        input [1:0] controlFlagsIn,
        input transmitDataEnable,
        input [7:0] transmitData,
        input transmitDataControlFlag,
        output transmitReady,
        input [5:0] transmitClockDivideValue,
        output [5:0] creditCount,
        output [5:0] outstandingCount,

        // receiver
        input receiveClock,
        output tickOut,
        output [5:0] timeOut,
        output [1:0] controlFlagsOut,
        output receiveFIFOWriteEnable1,
        output [7:0] receiveData,
        output receiveDataControlFlag,
        input [5:0] receiveFIFOCount,

        // serial IO
        output spaceWireDataOut,
        output spaceWireStrobeOut,
        input spaceWireDataIn,
        input spaceWireStrobeIn,
        input statisticalInformationClear,
        output [31:0] statisticalInformation0,
        output [31:0] statisticalInformation1,
        output [31:0] statisticalInformation2,
        output [31:0] statisticalInformation3,
        output [31:0] statisticalInformation4,
        output [31:0] statisticalInformation5,
        output [31:0] statisticalInformation6,
        output [31:0] statisticalInformation7

    );

    wire gotFCT;
    wire gotTimeCode;
    wire gotNCharacter;
    wire gotNull;
    wire iGotBit;
    wire iCreditError;
    wire parityError;
    wire escapeError;
    wire disconnectError;
    wire receiveError;
    wire enableReceive;
    wire sendNCharactors;
    wire sendTimeCode;
    wire after12p8us;
    wire after6p4us;
    wire enableTransmit;
    wire sendNulls;
    wire sendFCTs;
    wire spaceWireResetOutSignal;
    wire characterSequenceError;
    wire timer6p4usReset;
    wire timer12p8usStart;
    wire receiveFIFOWriteEnable0;
    wire iReceiveFIFOWriteEnable1;
    wire receiveOff;
    wire [7:0] receiveTimeCodeOut;
    wire linkUpTransitionSynchronize;
    wire linkDownTransitionSynchronize;
    wire linkUpEnable;
    wire nullSynchronize;
    wire fctSynchronize;
    wire receiveEEPAsynchronous;
    wire receiveEOPAsynchronous;
    wire receiveByteAsynchronous;
    wire transmitEEPAsynchronous;
    wire transmitEOPAsynchronous;
    wire transmitByteAsynchronous;
    wire [6:0] characterMonitor;

    assign receiveFIFOWriteEnable1  = iReceiveFIFOWriteEnable1;
    assign iReceiveFIFOWriteEnable1 = ( receiveFIFOWriteEnable0 & sendNCharactors );
    assign iGotBit                  = ~receiveOff;
    assign spaceWireResetOut        = spaceWireResetOutSignal;

    // Define status signal as LinkStatus or ErrorStatus
    assign linkStatus[0] = enableTransmit;
    assign linkStatus[1] = enableReceive;
    assign linkStatus[2] = sendNulls;
    assign linkStatus[3] = sendFCTs;
    assign linkStatus[4] = sendNCharactors;
    assign linkStatus[5] = sendTimeCode;
    assign linkStatus[6] = 0;
    assign linkStatus[7] = spaceWireResetOutSignal;
    assign linkStatus[15:8] = { 1'b0, characterMonitor };

    assign errorStatus[0] = characterSequenceError;  // sequence.
    assign errorStatus[1] = iCreditError;            // credit.
    assign errorStatus[2] = receiveError;            // receiveError(=parity, discon or escape error)
    assign errorStatus[3] = 0;
    assign errorStatus[4] = parityError;             // parity.
    assign errorStatus[5] = disconnectError;         // disconnect.
    assign errorStatus[6] = escapeError;             // escape.
    assign errorStatus[7] = 0;


    // instantiate components
    receiver_synchronize #( gDisconnectCountValue ) _SPACE_WIRE_RECEIVER (

        .receiveClock( receiveClock ),
        .spaceWireDataIn( spaceWireDataIn ),
        .spaceWireStrobeIn( spaceWireStrobeIn ),
        // .receiveDataOut[7:0]( receiveData ),
        // .receiveDataOut[8]( receiveDataControlFlag ),
        .receiveDataOut( { receiveDataControlFlag, receiveData } ),
        .receiveDataValidOut( receiveByteAsynchronous ),
        .receiveTimeCodeOut( receiveTimeCodeOut ),
        .receiveFIFOWriteEnable( receiveFIFOWriteEnable0 ),
        .receiveFCTOut( gotFCT ),
        .receiveTimeCodeValidOut( gotTimeCode ),
        .receiveNCharacterOut( gotNCharacter ),
        .receiveNullOut( gotNull ),
        .receiveEEPOut( receiveEEPAsynchronous ),
        .receiveEOPOut( receiveEOPAsynchronous ),
        .receiveOffOut( receiveOff ),
        .receiverErrorOut( receiveError ),
        .parityErrorOut( parityError ),
        .escapeErrorOut( escapeError ),
        .disconnectErrorOut( disconnectError ),
        .enableReceive( enableReceive ),
        .spaceWireReset( spaceWireResetOutSignal ) 

    );

    transmitter #( gTransmitClockDivideValue ) _SPACE_WIRE_TRANSMITTER (

        .transmitClock( transmitClock ),
        .clock( clock ),
        .receiveClock( receiveClock ),
        .reset( reset ),
        .spaceWireDataOut( spaceWireDataOut ),
        .spaceWireStrobeOut( spaceWireStrobeOut ),
        .tickIn( tickIn ),
        .timeIn( timeIn ),
        .controlFlagsIn( controlFlagsIn ),
        .transmitDataEnable( transmitDataEnable ),
        .transmitData( transmitData ),
        .transmitDataControlFlag( transmitDataControlFlag ),
        .transmitReady( transmitReady ),
        .enableTransmit( enableTransmit ),
        // autoStart
        .sendNulls( sendNulls ),
        .sendFCTs( sendFCTs ),
        .sendNCharacters( sendNCharactors ),
        .sendTimeCodes( sendTimeCode ),
        // tx_fct
        .gotFCT( gotFCT ),
        .gotNCharacter( gotNCharacter ),
        .receiveFIFOCount( receiveFIFOCount ),
        .creditError( iCreditError ),
        .transmitClockDivide( transmitClockDivideValue ),
        .creditCountOut( creditCount ),
        .outstandingCountOut( outstandingCount ),
        .spaceWireResetOut( spaceWireResetOutSignal ),
        .transmitEEPAsynchronous( transmitEEPAsynchronous ),
        .transmitEOPAsynchronous( transmitEOPAsynchronous ),
        .transmitByteAsynchronous( transmitByteAsynchronous )    

    );

    state_machine _SPACE_WIRE_STATE_MACHINE (

        .Clock( clock ),
        .receiveClock( receiveClock ),
        .reset( reset ),
        .after12p8us( after12p8us ),
        .after6p4us( after6p4us ),
        .linkStart( linkStart ),
        .linkDisable( linkDisable ),
        .autoStart( autoStart ),
        .enableTransmit( enableTransmit ),
        .sendNulls( sendNulls ),
        .sendFCTs( sendFCTs ),
        .sendNCharacter( sendNCharactors ),
        .sendTimeCodes( sendTimeCode ),
        .gotFCT( gotFCT ),
        .gotTimeCode( gotTimeCode ),
        .gotNCharacter( gotNCharacter ),
        .gotNull( gotNull ),
        .gotBit( iGotBit ),
        .creditError( iCreditError ),
        .receiveError( receiveError ),
        .enableReceive( enableReceive ),
        .characterSequenceError( characterSequenceError ),
        .spaceWireResetOut( spaceWireResetOutSignal ),
        .FIFOAvailable( FIFOAvailable ),
        .timer6p4usReset( timer6p4usReset ),
        .timer12p8usStart( timer12p8usStart ),
        .linkUpTransitionSynchronize( linkUpTransitionSynchronize ),
        .linkDownTransitionSynchronize( linkDownTransitionSynchronize ),
        .linkUpEnable( linkUpEnable ),
        .nullSynchronize( nullSynchronize ),
        .fctSynchronize( fctSynchronize )

    );

    timer #( gTimer6p4usValue, gTimer12p8usValue ) _SPACE_WIRE_TIMER (

        .clock( clock ),
        .reset( reset ),
        .timer6p4usReset( timer6p4usReset ),
        .timer12p8usStart( timer12p8usStart ),
        .after6p4us( after6p4us ),
        .after12p8us( after12p8us ) 

    );

    statistical_information_count _SPACE_WIRE_SIC (

        .clock( clock ),
        .reset( reset ),
        .statisticalInformationClear( statisticalInformationClear ),
        .transmitClock( transmitClock ),
        .receiveClock( receiveClock ),
        .receiveEEPAsynchronous( receiveEEPAsynchronous ),
        .receiveEOPAsynchronous( receiveEOPAsynchronous ),
        .receiveByteAsynchronous( receiveByteAsynchronous ),
        .transmitEEPAsynchronous( transmitEEPAsynchronous ),
        .transmitEOPAsynchronous( transmitEOPAsynchronous ),
        .transmitByteAsynchronous( transmitByteAsynchronous ),
        .linkUpTransition( linkUpTransitionSynchronize ),
        .linkDownTransition( linkDownTransitionSynchronize ),
        .linkUpEnable( linkUpEnable ),
        .nullSynchronous( nullSynchronize ),
        .fctSynchronous( fctSynchronize ),
        .statisticalInformation0( statisticalInformation0 ),
        .statisticalInformation1( statisticalInformation1 ),
        .statisticalInformation2( statisticalInformation2 ),
        .statisticalInformation3( statisticalInformation3 ),
        .statisticalInformation4( statisticalInformation4 ),
        .statisticalInformation5( statisticalInformation5 ),
        .statisticalInformation6( statisticalInformation6 ),
        .statisticalInformation7( statisticalInformation7 ),
        .characterMonitor( characterMonitor )

    );

    time_code_control _SPACE_WIRE_TCC (

        .clock( clock ),
        .reset( reset ),
        .receiveClock( receiveClock ),
        .gotTimeCode( gotTimeCode ),
        .receiveTimeCodeOut( receiveTimeCodeOut ),
        .timeOut( timeOut ),
        .controlFlagsOut( controlFlagsOut ),
        .tickOut( tickOut )

    );

endmodule