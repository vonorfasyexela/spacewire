`timescale 1ns / 1ps

module state_machine_tb(

        output reg Clock,
        output reg receiveClock,
        output reg reset,
        output reg after12p8us,
        output reg after6p4us,
        output reg linkStart,
        output reg linkDisable,
        output reg autoStart,

        input enableTransmit,
        input sendNulls,
        input sendFCTs,
        input sendNCharacter,
        input sendTimeCodes,

        output reg gotFCT,
        output reg gotTimeCode,
        output reg gotNCharacter,
        output reg gotNull,
        output reg gotBit,
        output reg creditError,
        output reg receiveError,

        input enableReceive,
        input characterSequenceError,
        input spaceWireResetOut,

        output reg FIFOAvailable,

        input timer6p4usReset,
        input timer12p8usStart,
        input linkUpTransitionSynchronize,
        input linkDownTransitionSynchronize,
        input linkUpEnable,
        input nullSynchronize,
        input fctSynchronize

    );

    state_machine _STATE_MACHINE (

        .Clock( Clock ),
        .receiveClock( receiveClock ),
        .reset( reset ),
        .after12p8us( after12p8us ),
        .after6p4us( after6p4us ),
        .linkStart( linkStart ),
        .linkDisable( linkDisable ),
        .autoStart( autoStart ),

        .enableTransmit( enableTransmit ),
        .sendNulls( sendNulls ),
        .sendFCTs( sendFCTs ),
        .sendNCharacter( sendNCharacter ),
        .sendTimeCodes( sendTimeCodes ),

        .gotFCT( gotFCT ),
        .gotTimeCode( gotTimeCode ),
        .gotNCharacter( gotNCharacter ),
        .gotNull( gotNull ),
        .gotBit( gotBit ),
        .creditError( creditError ),
        .receiveError( receiveError ),

        .enableReceive( enableReceive ),
        .characterSequenceError( characterSequenceError ),
        .spaceWireResetOut( spaceWireResetOut ),

        .FIFOAvailable( FIFOAvailable ),

        .timer6p4usReset( timer6p4usReset ),
        .timer12p8usStart( timer12p8usStart ),
        .linkUpTransitionSynchronize( linkUpTransitionSynchronize ),
        .linkDownTransitionSynchronize( linkDownTransitionSynchronize ),
        .linkUpEnable( linkUpEnable ),
        .nullSynchronize( nullSynchronize ),
        .fctSynchronize( fctSynchronize )

    );

    initial begin
        $dumpfile( "state_machine_tb.vcd" );
        $dumpvars( 0, state_machine_tb );

        Clock = 0;
        reset = 1;

        #10 reset = 0;

    end

    initial
        #3000 $finish;

    always
        #5 Clock = !Clock;

endmodule