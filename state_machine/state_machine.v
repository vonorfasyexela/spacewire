module state_machine (

        input Clock,
        input receiveClock,
        input reset,
        input after12p8us,
        input after6p4us,
        input linkStart,
        input linkDisable,
        input autoStart,

        output enableTransmit,
        output sendNulls,
        output sendFCTs,
        output sendNCharacter,
        output sendTimeCodes,

        input gotFCT,
        input gotTimeCode,
        input gotNCharacter,
        input gotNull,
        input gotBit,
        input creditError,
        input receiveError,

        output enableReceive,
        output characterSequenceError,
        output spaceWireResetOut,

        input FIFOAvailable,

        output timer6p4usReset,
        output timer12p8usStart,
        output linkUpTransitionSynchronize,
        output linkDownTransitionSynchronize,
        output linkUpEnable,
        output nullSynchronize,
        output fctSynchronize

    );

    // states
    localparam [2:0]                                  linkStateErrorReset = 0,
                                                       linkStateErrorWait = 1,
                                                           linkStateReady = 2,
                                                         linkStateStarted = 3, 
                                                      linkStateConnecting = 4,
                                                             linkStateRun = 5;
    reg [2:0] linkState;

    // signals
    wire gotNullSynchronize;
    wire gotFCTSynchronize;
    wire gotTimeCodeSynchronize;
    wire gotNCharacterSynchronize;
    wire iAsynchronousError;
    wire receiveErrorsSynchronize;
    reg iCharacterSequenceError;
    reg iEnableTransmit;
    reg iSendNulls;
    reg iSendFCTs;
    reg iSendNCharacter;
    reg iSendTimeCodes;
    reg iEnableReceive;
    reg iSpaceWireResetOut;
    reg iTimer6p4usReset;
    reg iTimer12p8usStart;
    //
    reg iLinkUpTransition;
    reg iLinkDownTransition;
    reg iLinkUpEnable;
    reg creditSynchronize;

    assign characterSequenceError        = iCharacterSequenceError;
    assign enableTransmit                = iEnableTransmit;
    assign sendNulls                     = iSendNulls;
    assign sendFCTs                      = iSendFCTs;
    assign sendNCharacter                = iSendNCharacter;
    assign sendTimeCodes                 = iSendTimeCodes;
    assign enableReceive                 = iEnableReceive;
    assign spaceWireResetOut             = iSpaceWireResetOut;
    assign timer6p4usReset               = iTimer6p4usReset;
    assign timer12p8usStart              = iTimer12p8usStart;
    assign linkUpTransitionSynchronize   = iLinkUpTransition;
    assign linkDownTransitionSynchronize = iLinkDownTransition;
    assign linkUpEnable                  = iLinkUpEnable;
    assign nullSynchronize               = gotNullSynchronize;
    assign fctSynchronize                = gotFCTSynchronize;

    assign iAsynchronousError            = receiveErrorsSynchronize; //

    // serializers
    synchronize_one_pulse _GOT_NULL_PULSE (
        .clock( Clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( gotNull ),
        
        .synchronizedOut( gotNullSynchronize )
    );

    synchronize_one_pulse _GOT_FCT_PULSE (
        .clock( Clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( gotFCT ),
        
        .synchronizedOut( gotFCTSynchronize )
    );

    synchronize_one_pulse _GOT_TIME_CODE_PULSE (
        .clock( Clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( gotTimeCode ),
        
        .synchronizedOut( gotTimeCodeSynchronize )
    );

    synchronize_one_pulse _GOT_NCHARACTER_PULSE (
        .clock( Clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( gotNCharacter ),
        
        .synchronizedOut( gotNCharacterSynchronize )
    );

    synchronize_one_pulse _ERROR_PULSE (
        .clock( Clock ),
        .asynchronousClock( receiveClock ),
        .reset( reset ),
        .asynchronousIn( receiveError ),
        
        .synchronizedOut( receiveErrorsSynchronize )
    );


    // ECSS-E-ST-50-12C 8.4.6   StateMachine.
    // ECSS-E-ST-50-12C 8.5.3.7 RxErr.
    // ECSS-E-ST-50-12C 8.5.3.8 CreditError.
    always @( posedge Clock or posedge reset or posedge creditError ) begin
        // if ( reset == 1 || creditError == 1 ) begin
        if ( reset == 1 ) begin
            linkState               = linkStateErrorReset;
            iSpaceWireResetOut      = 1;
            iEnableReceive          = 0;
            iEnableTransmit         = 0;
            iSendNulls              = 0;
            iSendFCTs               = 0;
            iSendNCharacter         = 0;
            iSendTimeCodes          = 0;
            iCharacterSequenceError = 0;
            iTimer6p4usReset        = 1;
            iTimer12p8usStart       = 0;
            iLinkDownTransition     = 0;
            iLinkUpTransition       = 0;
            iLinkUpEnable           = 0;
        end
        else if ( creditError == 1 ) begin
            linkState               = linkStateErrorReset;
            iSpaceWireResetOut      = 1;
            iEnableReceive          = 0;
            iEnableTransmit         = 0;
            iSendNulls              = 0;
            iSendFCTs               = 0;
            iSendNCharacter         = 0;
            iSendTimeCodes          = 0;
            iCharacterSequenceError = 0;
            iTimer6p4usReset        = 1;
            iTimer12p8usStart       = 0;
            iLinkDownTransition     = 0;
            iLinkUpTransition       = 0;
            iLinkUpEnable           = 0; 
        end

        else begin
            
            case ( linkState )

                // ECSS-E-ST-50-12C 8.5.2.2 ErrorReset.
                // When the reset signal is de-asserted the ErrorReset state 
                // shall be left
                // unconditionally after a delay of 6,4 us (nominal) and the 
                // state machine
                // shall move to the ErrorWait state.
                linkStateErrorReset: begin
                    iLinkUpEnable = 0;
                    iLinkDownTransition = ( iSendTimeCodes == 1 ) ? 1 : 0;
                    if ( FIFOAvailable == 1 ) begin
                        iTimer6p4usReset = 0;
                    end

                    iSpaceWireResetOut      = 1;
                    iEnableReceive          = 0;
                    iEnableTransmit         = 0;
                    iSendNulls              = 0;
                    iSendFCTs               = 0;
                    iSendNCharacter         = 0;
                    iSendTimeCodes          = 0;
                    iCharacterSequenceError = 0;

                    if ( receiveErrorsSynchronize == 1 ) begin
                        linkState = linkStateErrorReset;
                    end
                    else if ( after6p4us == 1 ) begin
                        iTimer12p8usStart = 1;
                        linkState = linkStateErrorWait;
                    end
                end

                // ECSS-E-ST-50-12C 8.5.2.3 ErrorWait.
                // The ErrorWait state shall be left unconditionally after a 
                // delay of 12,8 us
                // (nominal) and the state machine shall move to the Ready 
                // state.
                // If, while in the ErrorWait state, a disconnection error is 
                // detected
                // the state machine shall move back to the ErrorReset state.
                linkStateErrorWait: begin
                    iSpaceWireResetOut = 0;
                    iTimer12p8usStart  = 0;
                    iEnableReceive     = 1;
                    
                    if ( receiveErrorsSynchronize == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState = linkStateErrorReset;
                    end
                    else if ( gotTimeCodeSynchronize == 1 || 
                              gotFCTSynchronize == 1 ||
                              gotNCharacterSynchronize == 1 ) begin
                    
                        iCharacterSequenceError = 1;
                        iTimer6p4usReset        = 1;
                        linkState               = linkStateErrorReset;
                    end
                    else if ( after12p8us == 1 ) begin
                        linkState = linkStateReady;
                    end
                end

                // ECSS-E-ST-50-12C 8.5.2.4 Ready.
                // The state machine shall wait in the Ready state until 
                // the [Link Enabled]
                // guard becomes true and then it shall move on into the 
                // Started state.
                // If, while in the Ready state, a disconnection error is 
                // detected, or if 
                // after thegotNULL condition is set, a parity error or 
                // escape error occurs, 
                // or any character other than a NULL is received, then 
                // the state machine 
                // shall move to the ErrorReset state.
                linkStateReady: begin
                    iEnableReceive = 1;
                    if ( receiveErrorsSynchronize == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                    else if ( gotFCTSynchronize == 1 ||
                              gotNCharacterSynchronize == 1 ||
                            gotTimeCodeSynchronize == 1 ) begin
                    
                        iCharacterSequenceError = 1;
                        iTimer6p4usReset        = 1;
                        linkState               = linkStateErrorReset;
                    end
                    else if ( autoStart == 1 && 
                              gotNullSynchronize == 1 ) begin
                        iTimer12p8usStart = 1;
                        linkState = linkStateStarted;
                    end
                    else if ( linkStart == 1 ) begin
                        iTimer12p8usStart = 1;
                        linkState = linkStateStarted;
                    end
                end

                // ECSS-E-ST-50-12C 8.5.2.5 Started.
                // The state machine shall move to the Connecting state if 
                // the gotNULL
                // condition is set.
                // If, while in the Started state, a disconnection error is 
                // detected, or if 
                // after the gotNULL condition is set, a parity error or 
                // escape error occurs, 
                // or any character other than a NULL is received, then 
                // the state machine shall
                // move to the ErrorReset state.
                linkStateStarted: begin
                    iEnableTransmit   = 1;
                    iEnableReceive    = 1;
                    iSendNulls        = 1;
                    iTimer12p8usStart = 0;

                    if ( receiveErrorsSynchronize == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                    else if ( linkDisable == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                    else if ( gotFCTSynchronize == 1 ||
                              gotNCharacterSynchronize == 1 || 
                              gotTimeCodeSynchronize == 1 ) begin
                    
                        iCharacterSequenceError = 1;
                        iTimer6p4usReset        = 1;
                        linkState               = linkStateErrorReset;
                    end
                    else if ( after12p8us == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                    else if ( gotNullSynchronize == 1 ) begin
                        iTimer12p8usStart = 1;
                        linkState         = linkStateConnecting;
                    end
                end

                // ECSS-E-ST-50-12C 8.5.2.6 Connecting
                // If an FCT is received (gotFCT condition true) the state 
                // machine shall
                // move to the Run state.
                // If, while in the Connecting state, a disconnect error, 
                // parity error or 
                // escape error is detected, or if any character other 
                // than NULL or 
                // FCT is received, then the state machine shall move to 
                // the ErrorReset 
                // state.
                linkStateConnecting: begin
                    iTimer12p8usStart = 0;
                    iEnableTransmit   = 1;
                    iEnableReceive    = 1;
                    iSendFCTs         = 1;

                    if ( receiveErrorsSynchronize == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState = linkStateErrorReset;
                    end
                    else if ( linkDisable == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                    else if ( after12p8us == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end                    
                    else if ( gotNCharacterSynchronize == 1 ) begin
                        iCharacterSequenceError = 1;
                        iTimer6p4usReset        = 1;
                        linkState               = linkStateErrorReset;
                    end
                    else if ( gotFCTSynchronize == 1 ) begin
                        linkState = linkStateRun;
                    end
                end

                // ECSS-E-ST-50-12C 8.5.2.7 Run
                // In the Run state the receiver is enabled and 
                // the transmitter is 
                // enabled to send Time-Codes, FCTs, N-Chars and NULLs.
                // If  a disconnection error, parity error, ESC error occur, 
                // then the state machine
                // shall move to the ErrorResetState.
                linkStateRun: begin
                    iEnableTransmit = 1;
                    iEnableReceive  = 1;
                    iSendNCharacter = 1;
                    iSendTimeCodes  = 1;
                    iLinkUpEnable   = 1;

                    iLinkUpTransition = ( iSendTimeCodes == 0 ) ? 1 : 0;

                    if ( linkDisable == 1 || 
                         receiveErrorsSynchronize == 1 ) begin
                        iTimer6p4usReset = 1;
                        linkState        = linkStateErrorReset;
                    end
                end
            endcase
        end
    end    
endmodule