`timescale 1ns / 1ps

module synchronize_one_pulse_tb (

        output reg clock,
        output reg asynchronousClock,
        output reg reset,
        output reg asynchronousIn,
        input synchronizedOut
    
    );

    synchronize_one_pulse _SYNCHRONIZE_ONE_PULSE (
    
        .clock( clock ),
        .asynchronousClock( asynchronousClock ),
        .reset( reset ),
        .asynchronousIn( asynchronousIn ),
        .synchronizedOut( synchronizedOut )
    
    );

    initial begin   

        $dumpfile( "synchronize_one_pulse_tb.vcd" );
        $dumpvars( 0, synchronize_one_pulse_tb );

        clock = 0;
        reset = 1;
        asynchronousClock = 1;
        asynchronousIn = 1;
    end

    initial begin
        #5 reset = 1;
        #5 reset = 0;
        asynchronousIn = 1;      
        #10 asynchronousIn = 0;
        #20 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #30 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #20 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #40 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #60 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #20 asynchronousIn = 1;
        #10 asynchronousIn = 0;
        #10 asynchronousIn = 1;
        #10 asynchronousIn = 0;
    end
        
    always
        #5 clock = !clock;
        
    always      
        #5 asynchronousClock = !asynchronousClock;
        
    initial 
        #3000 $finish;
        
endmodule
