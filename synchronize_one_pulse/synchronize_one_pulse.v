module synchronize_one_pulse (
    
        input clock,
        input asynchronousClock,
        input reset,
        input asynchronousIn,
        output synchronizedOut
    
    );
        
    reg iLatchedAsynchronous;
    reg iSynchronousRegister;
    reg iSynchronousClear;
    reg iSynchronizedOut;

    /*    
        Synchronize the asynchronous One Shot Pulse to Clock
    */

    assign synchronizedOut = iSynchronizedOut;

    // Latch the rising edge of the input signal
    always @( posedge asynchronousIn or 
              posedge reset or 
              posedge iSynchronousClear ) begin
     
        if ( reset == 1 ) begin
            iLatchedAsynchronous = 0;
        end
        else if ( iSynchronousClear == 1 ) begin
            iLatchedAsynchronous = 0;
        end
        else begin
            iLatchedAsynchronous = 1;
        end
    end

    // Synchronize a latch signal to Clock
    always @( posedge clock or 
              posedge reset or 
              posedge iSynchronousClear ) begin

        if ( reset == 1 ) begin
            iSynchronousRegister = 0;
        end
        else if ( iSynchronousClear == 1 ) begin
            iSynchronousRegister = 0;
        end
        else if ( iLatchedAsynchronous == 1 ) begin
            iSynchronousRegister = 1;
        end
    end
    
    // Output Clock synchronized One_Shot_Pulse and clear signal
    always @( posedge clock or 
              posedge reset ) begin

        if ( reset == 1 ) begin
            iSynchronizedOut  = 0;
            iSynchronousClear = 0;             
        end 
        else begin
            if ( iSynchronousRegister == 1 && iSynchronousClear == 0 ) begin
                iSynchronizedOut  = 1;
                iSynchronousClear = 1;                     
            end
            else if ( iSynchronousRegister == 1 ) begin 
                iSynchronizedOut  = 0;
                iSynchronousClear = 0;           
            end 
            else begin
                iSynchronizedOut  = 0;
                iSynchronousClear = 0;         
            end
        end
    end

endmodule
