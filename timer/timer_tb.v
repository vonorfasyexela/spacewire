`timescale 1ns / 1ps

module timer_tb (

        output reg clock,
        output reg reset,
        output reg timer6p4usReset,
        output reg timer12p8usStart,

        input after6p4us,
        input after12p8us

    );
    
    // По умолчанию создается таймер со значениями gTimer6p4usValue = 640, 
    // gTimer12p8usValue = 1280. Если частота тактирования нужна другая, то 
    // числа 6400 и 12800 (т.е. периоды таймера в наносекундах) делятся на 
    // период требуемой частоты и создается таймер с такими параметрами. 
    // Результаты моделирования будут такими же.
    timer _TIMER (

        .clock( clock ),
        .reset( reset ),
        .timer6p4usReset( timer6p4usReset ),
        .timer12p8usStart( timer12p8usStart ),

        .after6p4us( after6p4us ),
        .after12p8us( after12p8us )

    );

    initial begin
        $dumpfile( "timer_tb.vcd" );
        $dumpvars( 0, timer_tb );

        clock = 0;
        reset = 1;
        timer6p4usReset = 0;
        timer12p8usStart = 0;        

        #20 reset = 0;

        // если не трогать таймер - генерирует сигнал каждые 6.4 мкс
        #15000;

        // попробуем сбросить таймер
        timer6p4usReset = 1;
        #20;
        timer6p4usReset = 0;

        #10000;

        // теперь включим второй таймер на 12.8 мкс
        timer12p8usStart = 1;
        #20;
        timer12p8usStart = 0;

    end

    initial
        #40000 $finish;

    always 
        // 5 нс - это полупериод. Период равен 10 нс. Умножаем 10 нс на 640 и 
        // получаем 6400 нс или 6.4 мкс
        #5 clock = !clock;

endmodule