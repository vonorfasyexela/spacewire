module timer #( parameter gTimer6p4usValue = 640, gTimer12p8usValue = 1280 ) (
        
        input clock,
        input reset,
        input timer6p4usReset,
        input timer12p8usStart,
        
        output after6p4us,
        output after12p8us

    );

    // signals 
    reg iTimerState12p8us;
    reg [9:0] iTimerCount6p4us;
    reg [10:0] iTimerCount12p8us;
    reg iAfter6p4us;
    reg iAfter12p8us;

    assign after6p4us  = iAfter6p4us;
    assign after12p8us = iAfter12p8us;

    // ECSS-E-ST-50-12C  8.4.7 Timer.
    // The timer provides the After 6.4 us and After 12.8 us timeouts used 
    // in link initialization.
    
    // After 6.4us.
    always @( posedge clock or 
              posedge reset or 
              posedge timer6p4usReset ) begin

        if ( reset == 1 ) begin
            iTimerCount6p4us <= 0;
            iAfter6p4us      <= 0;
        end
        else if ( timer6p4usReset == 1 ) begin
            iTimerCount6p4us <= 0;
            iAfter6p4us      <= 0; 
        end
        else begin
            if ( iTimerCount6p4us < gTimer6p4usValue ) begin
                iTimerCount6p4us <= iTimerCount6p4us + 1;
                iAfter6p4us      <= 0;
            end
            else begin
                iTimerCount6p4us <= 0;
                iAfter6p4us      <= 1;
            end
        end
    end
        
    // After 12.8us.
    always @( clock or 
              reset or 
              timer12p8usStart or 
              timer6p4usReset ) begin

        if ( reset == 1 ) begin
            iTimerState12p8us <= 0;
            iTimerCount12p8us <= 0;
            iAfter12p8us      <= 0;
        end
        else if ( timer6p4usReset == 1 ) begin
            iTimerState12p8us <= 0;
            iTimerCount12p8us <= 0;
            iAfter12p8us      <= 0; 
        end
        // else begin
        else if ( clock == 1 ) begin
            if ( iTimerState12p8us == 0 ) begin
                iAfter12p8us <= 0;              
                if (timer12p8usStart == 1) begin
                    iTimerState12p8us <= 1;
                end             
            end
            else begin
                if (iTimerCount12p8us < gTimer12p8usValue) begin
                    iTimerCount12p8us <= iTimerCount12p8us + 1;
                    iAfter12p8us      <= 0;
                end             
                else begin
                    iTimerCount12p8us <= 0;
                    iTimerState12p8us <= 0;
                    iAfter12p8us      <= 1;
                end
            end        
        end
    end
endmodule
