module spacewire #( parameter gDisconnectCountValue = 141, // transmitClock period * gDisconnectCountValue = 850ns
                              gTimer6p4usValue = 320,      // Clock period * gTimer6p4usValue = 6.4us
                              gTimer12p8usValue = 640,     // Clock period * gTimer12p8usValue = 12.8us
                              gInitializeTransmitClockDivideValue = 6'b001001 ) // transmitClock frequency / (gInitializeTransmitClockDivideValue + 1) = 10MHz

    (

        // системные сигналы
        input clock,
        input transmitClock,
        input receiveClock,
        input reset,

        // сигналы для работы с FIFO

        // transmit
        input transmitFIFOWriteEnable,
        input [8:0] transmitFIFODataIn,
        output transmitFIFOFull,
        output [5:0] transmitFIFODataCount,
        
        // receive
        input receiveFIFOReadEnable,
        output [8:0] receiveFIFODataOut,
        output receiveFIFOFull,
        output receiveFIFOEmpty,
        output [5:0] receiveFIFODataCount,
        //
        input tickIn,
        input [5:0] timeIn,
        input [1:0] controlFlagsIn,
        output tickOut,
        output [5:0] timeOut,
        output [1:0] controlFlagsOut,
        // 
        input linkStart,
        input linkDisable,
        input autoStart,
        output [15:0] linkStatus,
        output [7:0] errorStatus,
        input [5:0] transmitClockDivideValue,
        output [5:0] creditCount,
        output [5:0] outstandingCount,
        //
        output transmitActivity,
        output receiveActivity,
        // 
        output spaceWireDataOut,
        output spaceWireStrobeOut,
        input spaceWireDataIn,
        input spaceWireStrobeIn,
        // 
        input statisticalInformationClear,
        output [31:0] statisticalInformation0,
        output [31:0] statisticalInformation1,
        output [31:0] statisticalInformation2,
        output [31:0] statisticalInformation3,
        output [31:0] statisticalInformation4,
        output [31:0] statisticalInformation5,
        output [31:0] statisticalInformation6,
        output [31:0] statisticalInformation7

    );

    localparam [2:0]                            transmitterWriteStateIdle = 0,
                                              transmitterWriteStateWrite0 = 1,
                                              transmitterWriteStateWrite1 = 2,
                                              transmitterWriteStateReset0 = 3,
                                              transmitterWriteStateReset1 = 4,
                                              transmitterWriteStateReset2 = 5;

    reg [2:0] transmitterWriteState;

    // transmitter
    reg iTransmitDataEnable;
    wire [7:0] iTransmitData;
    wire iTransmitDataControlFlag;
    wire transmitReady;

    wire receiveFIFOWriteEnable1;
    wire [7:0] receiveData;
    wire receiveDataControlFlag;
    wire [5:0] receiveFIFOCount;
    reg iTransmitFIFOReadEnable;
    wire transmitFIFOEmpty;
    wire [8:0] transmitFIFOReadData;
    wire [8:0] iReceiveFIFOWriteData;
    wire iReceiveFIFOWriteEnable2;
    wire iSpaceWireResetOut;
    reg iResetReceiveFIFO;
    reg iMiddleOfTransmitPacket;
    reg iMiddleOfReceivePacket;
    reg iMiddleOfReceivePacketSynchronized;
    wire iFIFOAvailable;
    reg iReceiveFIFOWriteEEP;

    assign iTransmitData = transmitFIFOReadData[7:0];
    assign iTransmitDataControlFlag = transmitFIFOReadData[8];

    assign transmitActivity = iTransmitFIFOReadEnable;
    assign receiveActivity = receiveFIFOWriteEnable1;

    // instantiate modules
    fifo _TRANSMIT_FIFO (

        .readClock( clock ),
        .readEnable( iTransmitFIFOReadEnable ),
        .readDataOut( transmitFIFOReadData ),
        .writeClock( clock ),
        .writeEnable( transmitFIFOWriteEnable ),
        .writeDataIn( transmitFIFODataIn ),
        .empty( transmitFIFOEmpty ),
        .full( transmitFIFOFull ),
        .readDataCount( transmitFIFODataCount ),
        .writeDataCount(  ),
        .reset( reset )         

    );

    fifo _RECEIVE_FIFO (

        .readClock( clock ),
        .readEnable( receiveFIFOReadEnable ),
        .readDataOut( receiveFIFODataOut ),
        .writeClock( receiveClock ),
        .writeEnable( iReceiveFIFOWriteEnable2 ),
        .writeDataIn( iReceiveFIFOWriteData ),
        .empty( receiveFIFOEmpty ),
        .full( receiveFIFOFull ),
        .readDataCount( receiveFIFOCount ),
        .writeDataCount(  ),
        .reset( reset )           

    );

    synchronize_one_pulse _TRANSMIT_READY_PULSE (

        .clock( clock ),
        .asynchronousClock( transmitClock ),
        .reset( reset ),
        .asynchronousIn( iTransmitBusy ),
        .synchronizedOut( transmitBusySynchronized )    

    );

    link_interface #( gDisconnectCountValue, 
                      gTimer6p4usValue, 
                      gTimer12p8usValue, 
                      gInitializeTransmitClockDivideValue ) 
    _SPACE_WIRE_LINK_INTERFACE  (

        .clock( clock ),
        .reset( reset ),

        // state machine
        .transmitClock( transmitClock ),
        .linkStart( linkStart ),
        .linkDisable( linkDisable ),
        .autoStart( autoStart ),
        .linkStatus( linkStatus ),
        .errorStatus( errorStatus ),
        .spaceWireResetOut( iSpaceWireResetOut ),
        .FIFOAvailable( iFIFOAvailable ),

        // transmitter
        .tickIn( tickIn ),
        .timeIn( timeIn ),
        .controlFlagsIn( controlFlagsIn ),
        .transmitDataEnable( iTransmitDataEnable ),
        .transmitData( iTransmitData ),
        .transmitDataControlFlag( iTransmitDataControlFlag ),
        .transmitReady( transmitReady ),
        .transmitClockDivideValue( transmitClockDivideValue ),
        .creditCount( creditCount ),
        .outstandingCount( outstandingCount ),

        // receiver
        .receiveClock( receiveClock ),
        .tickOut( tickOut ),
        .timeOut( timeOut ),
        .controlFlagsOut( controlFlagsOut ),
        .receiveFIFOWriteEnable1( receiveFIFOWriteEnable1 ),
        .receiveData( receiveData ),
        .receiveDataControlFlag( receiveDataControlFlag ),
        .receiveFIFOCount( receiveFIFOCount ),
        
        // serial i/o
        .spaceWireDataOut( spaceWireDataOut ),
        .spaceWireStrobeOut( spaceWireStrobeOut ),
        .spaceWireDataIn( spaceWireDataIn ),
        .spaceWireStrobeIn( spaceWireStrobeIn ),
        .statisticalInformationClear( statisticalInformationClear ),
        .statisticalInformation0( statisticalInformation0 ),
        .statisticalInformation1( statisticalInformation1 ),
        .statisticalInformation2( statisticalInformation2 ),
        .statisticalInformation3( statisticalInformation3 ),
        .statisticalInformation4( statisticalInformation4 ),
        .statisticalInformation5( statisticalInformation5 ),
        .statisticalInformation6( statisticalInformation6 ),
        .statisticalInformation7( statisticalInformation7 ) 

    );

    assign iReceiveFIFOWriteData = ( iReceiveFIFOWriteEEP == 1 ) ? 
                                   9'b100000001 :
                                   { receiveDataControlFlag, receiveData };

    assign iReceiveFIFOWriteEnable2 = receiveFIFOWriteEnable1 | 
                                      iReceiveFIFOWriteEEP;

    assign receiveFIFODataCount = receiveFIFOCount;
    assign iFIFOAvailable = ( iMiddleOfTransmitPacket == 1 || iMiddleOfReceivePacketSynchronized == 1 ) ?
                            0 :
                            1;

    assign iTransmitBusy = !transmitReady;



    // ECSS-E-ST-50-12C 11.4  Link error recovery.
    // If previous character was NOT EOP, then add EEP (error end of
    // packet) to the receiver buffer, when detect Error(SpaceWireReset) while 
    // receiving the Receive packet.
    always @( posedge receiveClock or posedge reset ) begin
        if ( reset == 1 ) begin
            iMiddleOfReceivePacket <= 0;
            iReceiveFIFOWriteEEP   <= 0;
            iResetReceiveFIFO      <= 0;
        end
        else begin
            iResetReceiveFIFO <= ( iSpaceWireResetOut == 1 ) ? 1 : 0;

            if ( iResetReceiveFIFO == 1 ) begin
                if ( iMiddleOfReceivePacket == 1 ) begin
                    iMiddleOfReceivePacket <= 0;
                    iReceiveFIFOWriteEEP   <= 1;
                end
                else begin
                    iReceiveFIFOWriteEEP <= 0;
                end
            end
            else if ( receiveFIFOWriteEnable1 == 1 ) begin
                iMiddleOfReceivePacket <= ( iReceiveFIFOWriteData[8] == 1 ) ? 0 : 1;
            end
        end
    end    

    // ECSS-E-ST-50-12C 11.4 Link error recovery.
    // Delete data in the  transmitter buffer until the next EOP,
    // when detect  Error (SpaceWireReset) while sending
    // the Receive packet
    always @( posedge clock or posedge reset ) begin
        if ( reset == 1 ) begin
            iTransmitDataEnable     <= 0;
            iTransmitFIFOReadEnable <= 0;
            iMiddleOfTransmitPacket <= 0;
            transmitterWriteState   <= transmitterWriteStateIdle;
        end
        else begin
            
            case ( transmitterWriteState ) 

                transmitterWriteStateIdle: begin
                    if ( iSpaceWireResetOut == 1 && iMiddleOfTransmitPacket == 1 ) begin
                        transmitterWriteState <= transmitterWriteStateReset0;
                    end
                    else begin
                        if ( transmitFIFOEmpty == 0 && transmitReady == 1 ) begin
                            iTransmitFIFOReadEnable <= 1;
                            transmitterWriteState   <= transmitterWriteStateWrite0;
                        end
                    end
                end

                transmitterWriteStateWrite0: begin
                    iTransmitDataEnable     <= 1;
                    iTransmitFIFOReadEnable <= 0;
                    transmitterWriteState   <= transmitterWriteStateWrite1;
                end

                transmitterWriteStateWrite1: begin
                    iTransmitDataEnable <= 0;
                    if ( iSpaceWireResetOut == 1 ) begin
                        if ( transmitFIFOReadData[8] == 1 ) begin
                            iMiddleOfTransmitPacket <= 0;
                            transmitterWriteState   <= transmitterWriteStateIdle;
                        end
                        else begin
                            iMiddleOfTransmitPacket <= 1;
                            transmitterWriteState   <= transmitterWriteStateReset0;
                        end
                    end
                    else begin
                        if ( transmitBusySynchronized == 1 ) begin
                            iMiddleOfTransmitPacket <= ( transmitFIFOReadData[8] == 1 ) ? 0 : 1;
                            transmitterWriteState <= transmitterWriteStateIdle;
                        end
                    end
                end                

                transmitterWriteStateReset0: begin
                    if ( transmitFIFOEmpty == 0 ) begin
                        iTransmitFIFOReadEnable <= 1;
                        transmitterWriteState   <= transmitterWriteStateReset1;
                    end
                end

                transmitterWriteStateReset1: begin
                    iTransmitFIFOReadEnable <= 0;
                    transmitterWriteState   <= transmitterWriteStateReset2;
                end

                transmitterWriteStateReset2: begin
                    if ( transmitFIFOReadData[8] == 1 ) begin
                        iMiddleOfTransmitPacket <= 0;
                        transmitterWriteState   <= transmitterWriteStateIdle;
                    end
                    else begin
                        iMiddleOfTransmitPacket <= 1;
                        transmitterWriteState   <= transmitterWriteStateReset0;
                    end
                end

            endcase

        end
    end

    always @( posedge clock or posedge reset ) begin
        if ( reset == 1 ) begin
            iMiddleOfReceivePacketSynchronized <= 0;
        end
        else begin
            iMiddleOfReceivePacketSynchronized <= ( iMiddleOfReceivePacket == 1 ) ? 1 : 0;
        end
    end

endmodule